extensions [sound]

; Raza de tortugas que será usada en PSO
breed [particulas particula]

; Personas: responden a la funcion a optimizar
breed [personas persona]

breed [hos ho]

; Propiedades de las partículas PSO
particulas-own
[
  gusto
  v ; vector de velocidad instantánea de la partícula
  personal-mejor-val   ; mejor valor que ha encontrado
  personal-mejor-x     ; coordenada x del mejor valor
  personal-mejor-y     ; coordenada y del mejor valor
  personal-mejor-z     ; coordenada z del mejor valor
  personal-mejor-i     ; coordenada i del mejor valor
  personal-mejor-j     ; coordenada j del mejor valor
  personal-mejor-k     ; coordenada k del mejor valor
]

personas-own [
  diversion                             ;; Diversión actual de la persona
  gusto                                 ;; Gusto musical en 6 parámetros
]

globals [
  stage                                 ;; Imagen para el escenario
  afluencia                             ;; Cantidad de personas
  linea                                 ;; Linea de separación del escenario
  size-personas                         ;; Tamaña de las personas
  num-patron                            ;; Contador de patrones de la canción
  step                                  ;; Contador de pasos del patrón
  gradoGeneral                          ;; Grado base de la canción
  percutiva                             ;; Instrumentos pertenecientes a percusión
  melodica                              ;; Instrumentos pertenecientes a melodía
  menor                                 ;; Escala menor
  mayor                                 ;; Escala mayor
  
  ;;Variables globales a optimizar
  
  ; Variables globales usadas por el PSO:
  global-mejor-x      ; coordenada x del mejor valor encontrado por el PS
  global-mejor-y      ; coordenada y del mejor valor encontrado por el PS
  global-mejor-z      ; coordenada z del mejor valor encontrado por el PS
  global-mejor-i      ; coordenada x del mejor valor encontrado por el PS
  global-mejor-j      ; coordenada y del mejor valor encontrado por el PS
  global-mejor-k      ; coordenada y del mejor valor encontrado por el PS
  global-mejor-val    ; mejor valor encontrado por el PS
]

to setup
  clear-turtles
  clear-drawing
  reset-ticks
  sound:play-sound "beep.wav"
  set num-patron 0
  set step 0
  datos
  escenario
  publico
  hombreOrquesta
  set num-canales 0
end

to go
  reset-timer
  ask personas [set diversion diversion + funcion-caja-negra (bpm / 2) getValorEstilo estilo (triste-alegre * 100) percutiva-melodica randomize intensidad]
  if pso? = true [PSO]
  set num-patron num-patron + 1
  if num-patron mod 2 = 1 and num-canales < 15[set num-canales num-canales + 1]
  tick
  set step 0
  let patron getPatron getCanales
  repeat num-pasos [
    set step step + 1
    
    ;; Que suene cada instrumento en este paso
    foreach patron [ play ? ]
     
    ;; Público
    if bailar? = true [if step mod 4 = 1 [ask personas [bailar]]]
    
    ;; HO
    if bailar? = true [ask hos [set shape one-of (list "ho0" "ho1" "ho2" "ho3")]]
    
    ;; Control de tempo
    wait ( ( 60 / bpm ) / 4 ) - timer ;; Para controlar que el tiempo dedicado a ejecutar los sonidos no influya en la claqueta, pso se pasa de carga
    ;print timer
    
    reset-timer
  ]
  
end

to play [patron]
  
  ;; Percutivos
  if member? item 0 patron percutiva and item step patron > 0 [sound:play-drum getInstrumento item 0 patron intensidad ] 
  
  ;; Melódicos
  if member? item 0 patron melodica and item step patron > 0 [
    let nota armonizar item step patron item 0 patron
    sound:play-note (getInstrumento item 0 patron) nota (intensidad * 0.6) ((4 * 60 / bpm) / num-pasos)
    ]  
end

;; TODO TERCERAS; QUINTAS; OCTAVAS para dar mejor definicion a estilos
to-report armonizar [grado tipo]
  let escala []
  if-else triste-alegre = 0 [set escala menor][set escala mayor]
  if num-patron mod 4 = 1 and step = 1[set gradoGeneral 1 ] 
  if num-patron mod 4 != 1 and step = 1[set gradoGeneral random 9 + 1 ]
  set grado gradoGeneral
  let res tonalidad + item (grado - 1) escala
  ;let var random 7
  ;print var
  ;let variacion item var escala
  ;let tercera item (var + 2) escala
  ;set res res + ((random 2) * variacion)
  if tipo = "acc" [ set res res + 24 ]
  if tipo = "lea" [ set res tonalidad + item (random 9 + 1) escala + 48 ]
  
  ;print (word "acc " step " " res)
  report res
end

;; Determina el patron para un instrumento, determinado por su estilo, el tipo de sonido y su aleatoriedad
to-report getPatron [canales]
  let res []
 
  foreach canales [
    let aux getSecuencia ?
    if member? ? percutiva [set aux randomizer aux]
    set aux fput ? aux
    set res lput aux res

    ]
  report res
  
end

;; Determina los tipos de instrumentos para la cancion, depende de los canales
to-report getCanales 
  
  let res []
  let num-canales-melodicos round (num-canales * percutiva-melodica / 100)
  let num-canales-percutivos num-canales - num-canales-melodicos
  
  ;; Rellena canales percutivos
  let i 0
  repeat num-canales-percutivos [
    set res lput item i percutiva res
    set i i + 1
  ]
  
  ;; Rellena canales melódicos
  set i 0
  repeat num-canales-melodicos [
    set res lput item i melodica res
    set i i + 1
  ]
  ;set res (list "b-d" "sna" "h-h" "cym" "per" "tom" "bas" "acc" "lea" "efx" "atm")
  report res
end

; Determina el instrumento de un tipo dado que aparecerán en el patrón
to-report getInstrumento [tipo]
  ;; TODO estilo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  if-else estilo = "Reggaeton" [
    if-else tipo = "b-d" [report "Bass Drum 1"][
    if-else tipo = "sna" [report "Acoustic Snare"][
    if-else tipo = "h-h" [report "Pedal Hi Hat"][
    if-else tipo = "tom" [report one-of (list "low floor tom" "Low Timbale")][
    if-else tipo = "cym" [report "Ride Cymbal 1"][
    if-else tipo = "per" [report "Mute Cuica"][
    if-else tipo = "bas" [report "Synth Bass 2"][
    if-else tipo = "acc" [report "Synth Bass 1"][
    if-else tipo = "lea" [report "Marimba"][
    if-else tipo = "efx" [report "Bird Tweet"][
    if-else tipo = "atm" [report "Atmosphere"][
                        report "ERROR"]]]]]]]]]]]] [
  if-else estilo = "Pop" [
   if-else tipo = "b-d" [report "acoustic bass drum"][
    if-else tipo = "sna" [report "Acoustic Snare"][
    if-else tipo = "h-h" [report "closed hi hat"][
    if-else tipo = "tom" [report one-of (list "Hi Timbale" "Low Timbale")][
    if-else tipo = "cym" [report "splash cymbal"][
    if-else tipo = "per" [report "Mute Cuica"][
    if-else tipo = "bas" [report "Fingered Electric Bass"][
    if-else tipo = "acc" [report "Acoustic Grand Piano"][
    if-else tipo = "lea" [report "Electric Piano 1"][
    if-else tipo = "efx" [report "Bird Tweet"][
    if-else tipo = "atm" [report "Atmosphere"][
                        report "ERROR"]]]]]]]]]]] ] [
  if-else estilo = "Rock" [
    if-else tipo = "b-d" [report "acoustic bass drum"][
    if-else tipo = "sna" [report "acoustic snare"][
    if-else tipo = "h-h" [report one-of (list "closed hi hat" "open hi hat")][
    if-else tipo = "tom" [report one-of (list "Hi Timbale" "Low Timbale")][
    if-else tipo = "cym" [report "crash cymbal 1"][
    if-else tipo = "per" [report "Mute Cuica"][
    if-else tipo = "bas" [report "fretless bass"][
    if-else tipo = "acc" [report "Distortion Guitar"][
    if-else tipo = "lea" [report "guitar harmonics"][
    if-else tipo = "efx" [report "Bird Tweet"][
    if-else tipo = "atm" [report "Atmosphere"][
                        report "ERROR"]]]]]]]]]]]] [
  if-else estilo = "Techno" [
    if-else tipo = "b-d" [report "acoustic bass drum"][
    if-else tipo = "sna" [report one-of (list "hand clap")][
    if-else tipo = "h-h" [report "closed hi hat"][
    if-else tipo = "tom" [report one-of (list "Hi Timbale" "Low Timbale")][
    if-else tipo = "cym" [report "splash cymbal"][
    if-else tipo = "per" [report "Cowbell"][
    if-else tipo = "bas" [report "Synth Brass 1"][
    if-else tipo = "acc" [report "marimba"][
    if-else tipo = "lea" [report "guitar harmonics"][
    if-else tipo = "efx" [report "Bird Tweet"][
    if-else tipo = "atm" [report "Atmosphere"][
                        report "ERROR"]]]]]]]]]]]] [
  if-else estilo = "Drum&Bass" [
    if-else tipo = "b-d" [report "acoustic bass drum"][
    if-else tipo = "sna" [report "acoustic snare"][
    if-else tipo = "h-h" [report "closed hi hat"][
    if-else tipo = "tom" [report one-of (list "Hi Timbale" "hi floor tom")][
    if-else tipo = "cym" [report "splash cymbal"][
    if-else tipo = "per" [report "Mute Cuica"][
    if-else tipo = "bas" [report "fretless bass"][
    if-else tipo = "acc" [report "Celesta"][
    if-else tipo = "lea" [report "Celesta"][
    if-else tipo = "efx" [report "Bird Tweet"][
    if-else tipo = "atm" [report "Atmosphere"][
                        report "ERROR"]]]]]]]]]]]] [
  ]]]]]
  
end

;; Nueva tendencia de randomizer beta
to-report randomizerBeta [secuencia]
  let res secuencia
  ;; Para no saturar muchos los cananales que de por si tienen muy pocas notas/golpes
  let saturacion occurrences 1 res  
  let i 0
  foreach res [
    if ? = 0 and (random 100) * 2 * saturacion / num-pasos > 100 - randomize [set res replace-item i res random 2 ]
    if ? = 1 and (random 100) / 2 * saturacion / num-pasos > 100 - randomize [set res replace-item i res random 2 ]
    set i i + 1
  ]
  report res
end

to-report randomizer [secuencia]
  let res secuencia
  ;; Para no saturar muchos los cananales que de por si tienen muy pocas notas/golpes
  let saturacion occurrences 1 res  
  let desaturacion occurrences 0 res 
  let i 0
  foreach res [
    if saturacion != 0 [
      if (random 100) > 100 - randomize [ set res replace-item i res (random 5 - (3 - (3 * ?)))]
      ;if ? = 1 and (random 100) * 4 * saturacion / num-pasos > 100 - randomize [ set res replace-item i res random 3 ]
    ]
    set i i + 1
  ]
  report res
end

to-report randomizerBeta2 [secuencia]
  let res secuencia
  ;; Para no saturar muchos los cananales que de por si tienen muy pocas notas/golpes
  let saturacion occurrences 1 res  
  let i 0
  foreach res [
    if saturacion != 0 [
      if ? = 0 and (random 100) * 4 * saturacion / num-pasos < randomize [set res replace-item i res random 2 ]
      if ? = 1 and (random 100) * 2 * saturacion / num-pasos < randomize [set res replace-item i res random 2 ]
    ]
    set i i + 1
  ]
  report res
end

; Determina la secuencia base en el patrón para un determinado tipo de instrumento y estilo
to-report getSecuencia [tipo]
  
  if tipo = "cym" [ if-else num-patron mod 4 = 1 [report [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]] [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]]]
  ;if tipo = "lea" [report one-of (list [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1] [1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0] [1 0 1 1 0 1 1 0 0 1 1 0 1 0 1 1])]
  
  ;; Mini Base de datos de ritmos base
  if-else estilo = "Reggaeton" [
                               ; #1      #5      #9      #13  
    if-else tipo = "b-d" [report [1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0]][
    if-else tipo = "sna" [report [0 0 0 1 0 0 1 0 0 0 0 1 0 0 1 0]][
    if-else tipo = "h-h" [report [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]][
    if-else tipo = "tom" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "cym" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "per" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "atm" [report [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "efx" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "lea" [report [0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0]][
    ;if-else tipo = "lea" [report one-of (list [1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0] [1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0] [1 1 0 1 0 0 1 0 0 1 0 0 1 0 1 0])][
      
                         let bas [1 0 0 1 0 0 1 0 1 0 0 1 0 0 1 0]
    if-else tipo = "bas" or tipo = "acc" [report bas][
                          report [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]]]]]]]]]]]
    ][
  if-else estilo = "Pop" or estilo = "Rock" [
                               ; #1      #5      #9      #13  
    if-else tipo = "b-d" [report [1 0 0 0 0 0 0 0 1 0 1 0 0 0 0 0]][
    if-else tipo = "sna" [report [0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0]][
    if-else tipo = "h-h" [report [1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0]][
    if-else tipo = "tom" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "cym" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "per" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
                         let bas [1 0 0 0 1 0 0 0 1 0 1 0 1 0 1 0]
    if-else tipo = "bas" or tipo = "acc" [report bas][
    if-else tipo = "atm" [report [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "efx" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "lea" [report one-of (list [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1] [1 0 1 1 0 0 1 0 0 1 1 1 1 0 1 1] [1 0 1 1 0 1 1 0 1 1 1 0 1 1 1 1])][
      
                          report [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]]]]]]]]]]]
   ][
  if-else estilo = "Techno" [
                               ; #1      #5      #9      #13  
    if-else tipo = "b-d" [report [1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0]][
    if-else tipo = "sna" [report [0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0]][
    if-else tipo = "h-h" [report [0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0]][
    if-else tipo = "tom" [report one-of (list [0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0] [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0])][
    if-else tipo = "cym" [report one-of (list [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0])][
    if-else tipo = "per" [report [0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "atm" [report [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "efx" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
                         let bas [0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0]
                         
    if-else tipo = "lea" [report one-of (list [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1] [1 0 1 1 0 1 1 0 1 1 0 0 1 0 1 1] [1 0 1 1 0 1 1 0 0 1 1 0 1 1 1 1])][
    if-else tipo = "bas" or tipo = "acc" [report bas][
                          report [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]]]]]]]]]]]
    ][
  if-else estilo = "Drum&Bass" [
                               ; #1      #5      #9      #13  
    if-else tipo = "b-d" [report [1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0]][
    if-else tipo = "sna" [report [0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0]][
    if-else tipo = "h-h" [report [1 0 1 0 1 0 1 1 1 0 1 0 1 0 1 0]][
    if-else tipo = "tom" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "cym" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "per" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "atm" [report [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "efx" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    if-else tipo = "lea" [report [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]][
    ;;if-else tipo = "lea" [report one-of (list [1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0] [1 0 0 0 1 0 0 0 1 0 0 0 0 0 1 1] [1 0 0 0 0 1 1 0 0 0 1 0 0 0 1 1])][
                         let bas [1 0 0 0 0 0 1 0 1 0 1 0 0 0 0 0]
    if-else tipo = "bas" or tipo = "acc" [report bas][
                          report [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1]]]]]]]]]]]
    ][
  
  report 0]]]]
  
end

to escenario
  import-drawing stage
end

to publico
  create-personas afluencia [
    set shape "person" 
    set gusto getRandomGusto
    set size size-personas
    set diversion 0
    move-to patch random-pxcor ((random -8) + linea)]
end

to entrarPublico
   create-personas afluencia * 0.5 [
    set shape "person" 
    set gusto getRandomGusto
    set size size-personas
    set diversion 0
    move-to patch random-pxcor ((random -8) + linea)]
end

to hombreOrquesta
  create-hos 1 [
     set shape "HO0" 
     set size size-personas + 2
     move-to patch 0 (linea + 5)]
end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PSO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; El siguiente procedimiento prepara el experimento de PSO
to setupAvispas
  
  clear-plot
  
  set global-mejor-val 0
  set global-mejor-x 0 
  set global-mejor-y 0
  set global-mejor-z 0 
  set global-mejor-i 0 
  set global-mejor-j 0 
  set global-mejor-k 0 
  
  ; crear partículas
  create-particulas num-particulas
  [
    ;;set shape "circle"
    ;;set size .5
    ;;set color one-of base-colors
    ; Se sitúan aleatoriamente en el mundo
    hide-turtle
    set gusto getRandomGusto
    
    ; proporcionar a las partículas velocidades iniciales (vx y vy) con una distribución normal
    set v getRandomGusto
    
    ; Calculamos el valor inicial de la partícula, que vendrá dado por el modelo de coches con los 
    ; parámetros aceleracion/deceleracion asociados a las coordenadas de la partícula.
    ;  Observa que las coordenadas (x,y) de la partícula, que puede estar en un mundo de [-25,25]x[-25,25],
    ;  se transforman adecuadamente en un mundo [0, 0.01]x[0, 0.1]
    let val funcion-caja-negra  (item 0 gusto) (item 1 gusto) (item 2 gusto) (item 3 gusto) (item 4 gusto) (item 5 gusto)

    ; Los valores personales mejores son los actuales
    set personal-mejor-val val
    set personal-mejor-x item 0 gusto
    set personal-mejor-y item 1 gusto
    set personal-mejor-z item 2 gusto
    set personal-mejor-i item 3 gusto
    set personal-mejor-j item 4 gusto
    set personal-mejor-k item 5 gusto

    ;set label precision val 2
    
    ; se baja el lápiz para que se vea el recorrido realizado
    ;pd
  ]
end

; Función que permite usar el modelo de coches para relizar el cálculo. 
; Esta es la función que realmente se optimiza.
to-report funcion-caja-negra [x y z i j k]
  ;diversion provocada por los parámetros
  let res 0
  ;; desviacion maxima para provocar diversion
  let d+ 1.2
  let d- 0.8
  ask personas [
    let cont 0
    foreach (list x y z i j k) [
      let p [item cont gusto] of myself
      if-else ? * d- < p and p < ? * d+ [set res res + 1][set res res - 1]
      set cont cont + 1
    ]
  ]
  
  report res
  ; Para que vaya más rápido, se impide la actualización gráfica
  ;no-display
  ; Generación de los coches para este experimento
  ;setup-cars
  ; Se fijan los valores de aceleración y deceleración asociados a esta partícula  
  ;set acceleration x
  ;set deceleration y
  ; Se deja correr el modelo de coches 500 pasos
  ;repeat 500 [go-modelo]
  ;display

  ; Se devuelve la función que se quiere optimizar: la velocidad media de los coches
  ;report mean [speed] of cars
end

; La siguiente función muestra cómo se podría usar la funcion-caja-negra
; para repetir un experimento un número determinado de veces (n) y quedarnos con 
; la media de los resultados
to-report f-repetida [n x y z i j k]
  report mean (n-values n [funcion-caja-negra x y z i j k])
end

; Procdimiento principal del agoritmo de optimización PSO
to PSO
  ; Calculamos el valor asociado a cada partícula
  ask particulas [

    ; Calcula el valor ejecutando el modelo de coches. Véase el uso de esta función
    ; en el setup
    let val funcion-caja-negra (item 0 gusto) (item 1 gusto) (item 2 gusto) (item 3 gusto) (item 4 gusto) (item 5 gusto)
    
    ;set label precision val 2

    ; actualizar el "mejor valor personal" para cada partícula,
    ; si han encontrado un valor mejor que el que tenían almacenado
    if val > personal-mejor-val
    [
      set personal-mejor-val val
      set personal-mejor-x item 0 gusto
      set personal-mejor-y item 1 gusto
      set personal-mejor-z item 2 gusto
      set personal-mejor-i item 3 gusto
      set personal-mejor-j item 4 gusto
      set personal-mejor-k item 5 gusto
    ]
    ; Se actualiza el mejor-global si fuera necesario
    if global-mejor-val < personal-mejor-val
    [
      set global-mejor-val personal-mejor-val
      set global-mejor-x personal-mejor-x 
      set global-mejor-y personal-mejor-y
      set global-mejor-z personal-mejor-z 
      set global-mejor-i personal-mejor-i 
      set global-mejor-j personal-mejor-j 
      set global-mejor-k personal-mejor-k 
    ]
    ; si la partícula tiene el mejor-global, se resalta
    ;if global-mejor-val = val [watch-me]
  ]
  
  ; Se actualiza la posición/velocidad de cada partícula
  ask particulas
  [
    set v *v inercia-particula v

    ; Cambia la velocidad para ser atraído al "mejor valor personal" que la partícula ha encontrado
    ;facexy personal-mejor-x personal-mejor-y
    
    let dist norma (list personal-mejor-x personal-mejor-y personal-mejor-z personal-mejor-i personal-mejor-j personal-mejor-k)
    ;TODO-------------------------------------------------------------------------------------------------------------------------------------
    set v +v v (*v ((100 - inercia-particula) * atraccion-a-mejor-personal * (random-float 1.0) * dist) (list personal-mejor-x personal-mejor-y personal-mejor-z personal-mejor-i personal-mejor-j personal-mejor-k))

    ; Cambia la velocidad para ser atraído por el "mejor global" que ha sido encontrado
    ;facexy global-mejor-x global-mejor-y
    set dist norma (list global-mejor-x global-mejor-y global-mejor-z global-mejor-i global-mejor-j global-mejor-k)
    ;TODO-------------------------------------------------------------------------------------------------------------------------------------
    set v +v v (*v ((100 - inercia-particula) * atraccion-al-global-mejor * (random-float 1.0) * dist) (list global-mejor-x global-mejor-y global-mejor-z global-mejor-i global-mejor-j global-mejor-k))

    ; los límites de velocidad son necesarios porque estamos trabajando sobre un toro, lo que podría
    ; implicar que las partículas podrían girar alrededor del mundo a velocidades excesivamente altas.
    ;set v map [ifelse-value (abs ? > lim-vel-particulas) [sg ? * lim-vel-particulas][?]] v

    ; Actualizamos la posición de la partícula
    ;facexy (xcor + first v) (ycor + last v)
    ;fd norma v
  ]
  
  ; Se impide que las partículas se aproximen en exceso entre si  
  ;ask particulas [
   ; ask other particulas in-radius 2 [
    ;  face myself fd -1 / distance myself]
    ;]
  
  ;tick
  cancion
end



; Conjunto de funciones auxiliares (las vectoriales, permiten trabajar con más de 2 variables)

; Producto por escalar: k * (v1,v2,...,vn) = (k*v1, k*v2, ..., k*vn)
to-report *v [lambda v1]
  report map [lambda * ?] v1
end

; Suma de vetores: (u1, u2, ..., un) + (v1, v2, ..., vn) = (u1+v1, u2+v2, ..., un+vn)
to-report ++v [v1 v2 v3 v4 v5 v6]
  report (map [?1 + ?2 + ?3 + ?4 + ?5 + ?6] v1 v2 v3 v4 v5 v6)
end

to-report +v [v1 v2]
  report (map [?1 + ?2] v1 v2)
end

; Función Signo
to-report sg [x]
  report ifelse-value (x >= 0) [1][-1]
end

; Norma de un vector: Sqrt (v1^2 + v2^2 +...+ vn^2)
to-report norma [v1]
  report sqrt sum map [? * ?] v1
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                      ADORNOS                           ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Procedimiento de agentes;
to bailar
  move-to one-of neighbors with [ pycor < linea ] 
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                    FUNCIONES                             ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; count the number of occurrences of an item in a list
to-report occurrences [x the-list]
  report reduce
    [ifelse-value (?2 = x) [?1 + 1] [?1]] (fput 0 the-list)
end

to datos
  
  set gradoGeneral 1
  
  set percutiva (list "b-d" "h-h" "sna" "cym" "per" "tom" "tom" "h-h" "tom" "cym" "b-d" "sna" "per" "per" "cym")
  set melodica (list "bas" "acc" "bas" "efx" "atm" "lea" "acc" "efx" "efx" "bas" "acc" "lea" "efx" "atm" "acc" "efx")
  
  set menor (list 0 2 3 5 7 8 10 12 14 15 17 19 20 22)
  set mayor (list 0 2 4 5 7 9 11 12 14 16 17 19 21 22)
  
  ;;set menor (list 0 2 3 5 7 8 10)
  ;;set mayor (list 0 2 4 5 7 9 11)

end

to-report getRandomGusto
  report (list ((30 + random 70)) ((random 5 + 1) * 20) (random 2 * 100) (random 100 + 1) (random 100 + 1) (random 50 + 50))
end

to-report getValorEstilo [estiloIn]
  
  if estiloIn = "Reggaeton" [report 20]
  if estiloIn = "Pop" [report 40]
  if estiloIn = "Rock" [report 60]
  if estiloIn = "Techno" [report 80]
  if estiloIn = "Drum&Bass" [report 100]
  
end

to-report getEstiloValor [valorIn]
  if valorIn = 20 [report "Reggaeton"]
  if valorIn = 40 [report "Pop"]
  if valorIn = 60 [report "Rock"]
  if valorIn = 80 [report "Techno"]
  if valorIn = 100 [report "Drum&Bass"]
end

to cancion
  set bpm 2 * global-mejor-x      ; coordenada x del mejor valor encontrado por el PS
  set estilo getEstiloValor global-mejor-y     ; coordenada y del mejor valor encontrado por el PS
  if-else global-mejor-z < 50 [set triste-alegre 0][set triste-alegre 1]       ; coordenada z del mejor valor encontrado por el PS
  set percutiva-melodica global-mejor-i      ; coordenada x del mejor valor encontrado por el PS
  set randomize global-mejor-j      ; coordenada y del mejor valor encontrado por el PS
  set intensidad global-mejor-k      ; coordenada y del mejor valor encontrado por el PS
end
@#$#@#$#@
GRAPHICS-WINDOW
210
10
913
438
30
-1
11.361
1
10
1
1
1
0
0
0
1
-30
30
-34
0
0
0
1
ticks
30.0

BUTTON
1170
160
1225
193
NIL
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

SLIDER
915
45
1125
78
bpm
bpm
1
200
126
1
1
NIL
HORIZONTAL

CHOOSER
915
80
1125
125
estilo
estilo
"Reggaeton" "Pop" "Rock" "Techno" "Drum&Bass"
3

BUTTON
1025
160
1095
193
LiveAtKorea
set stage \"stage3.jpg\"\nset afluencia 300\nset linea -24\nset size-personas 2\nsetup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
105
370
210
471
NIL
step
17
1
25

MONITOR
0
370
105
471
patron
num-patron
17
1
25

SLIDER
1125
45
1325
78
randomize
randomize
0
100
14
1
1
NIL
HORIZONTAL

SLIDER
915
10
1020
43
num-canales
num-canales
1
15
15
1
1
NIL
HORIZONTAL

SLIDER
1125
10
1325
43
percutiva-melodica
percutiva-melodica
1
100
64
1
1
NIL
HORIZONTAL

SLIDER
915
125
1125
158
triste-alegre
triste-alegre
0
1
1
1
1
NIL
HORIZONTAL

SLIDER
1020
10
1125
43
num-pasos
num-pasos
1
16
16
1
1
NIL
HORIZONTAL

SLIDER
1125
80
1325
113
intensidad
intensidad
0
127
58
1
1
NIL
HORIZONTAL

SLIDER
1125
125
1325
158
tonalidad
tonalidad
0
100
33
1
1
NIL
HORIZONTAL

CHOOSER
0
10
210
55
percutiva-test
percutiva-test
"Acoustic Bass Drum" "Bass Drum 1" "Side Stick" "Acoustic Snare" "Hand Clap" "Electric Snare" "Low Floor Tom" "Closed Hi Hat" "Hi Floor Tom" "Pedal Hi Hat" "Low Tom" "Open Hi Hat" "Low Mid Tom" "Hi Mid Tom" "Crash Cymbal 1" "Hi Tom" "Ride Cymbal 1" "Chinese Cymbal" "Ride Bell" "Tambourine" "Splash Cymbal" "Cowbell" "Crash Cymbal 2" "Vibraslap" "Ride Cymbal 2" "Hi Bongo" "Low Bongo" "Mute Hi Conga" "Open Hi Conga" "Low Conga" "Hi Timbale" "Low Timbale" "Hi Agogo" "Low Agogo" "Cabasa" "Maracas" "Short Whistle" "Long Whistle" "Short Guiro" "Long Guiro" "Claves" "Hi Wood Block" "Low Wood Block" "Mute Cuica" "Open Cuica" "Mute Triangle" "Open Triangle"
0

CHOOSER
0
90
210
135
melodica-test
melodica-test
"===Piano===" "Acoustic Grand Piano" "Bright Acoustic Piano" "Electric Grand Piano" "Honky-tonk Piano" "Electric Piano 1" "Electric Piano 2" "Harpsichord" "Clavi" "===Chromatic Percussion===" "Celesta" "Glockenspiel" "Music Box" "Vibraphone" "Marimba" "Xylophone" "Tubular Bells" "Dulcimer" "===Organ===" "Drawbar Organ" "Percussive Organ" "Rock Organ" "Church Organ" "Reed Organ" "Accordion" "Harmonica" "Tango Accordion" "===Guitar====" "Nylon String Guitar" "Steel Acoustic Guitar" "Jazz Electric Guitar" "Clean Electric Guitar" "Muted Electric Guitar" "Overdriven Guitar" "Distortion Guitar" "Guitar harmonics" "===Bass===" "Acoustic Bass" "Fingered Electric Bass" "Picked Electric Bass" "Fretless Bass" "Slap Bass 1" "Slap Bass 2" "Synth Bass 1" "Synth Bass 2" "===Strings===" "Violin" "Viola" "Cello" "Contrabass" "Tremolo Strings" "Pizzicato Strings" "Orchestral Harp" "Timpani" "===Ensemble===" "String Ensemble 1" "String Ensemble 2" "Synth Strings 1" "Synth Strings 2" "Choir Aahs" "Voice Oohs" "Synth Voice" "Orchestra Hit" "===Brass===" "Trumpet" "Trombone" "Tuba" "Muted Trumpet" "French Horn" "Brass Section" "Synth Brass 1" "Synth Brass 2"
42

SLIDER
915
195
1090
228
inercia-particula
inercia-particula
0
1
0.6
.01
1
NIL
HORIZONTAL

SLIDER
915
230
1090
263
lim-vel-particulas
lim-vel-particulas
0
10
5.06
0.01
1
NIL
HORIZONTAL

SLIDER
915
265
1090
298
atraccion-a-mejor-personal
atraccion-a-mejor-personal
0
1
0.24
0.01
1
NIL
HORIZONTAL

SLIDER
915
300
1090
333
atraccion-al-global-mejor
atraccion-al-global-mejor
0
1
0.22
0.01
1
NIL
HORIZONTAL

PLOT
1095
200
1325
365
Valor del Mejor encontrado
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot global-mejor-val"

MONITOR
915
370
1030
415
Mejor BPM
global-mejor-x * 2
17
1
11

MONITOR
1030
370
1145
415
Mejor Estilo
getEstiloValor global-mejor-y
17
1
11

MONITOR
1145
370
1260
415
Mejor Sentimiento
global-mejor-z
17
1
11

BUTTON
1260
370
1325
460
NIL
PSO
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
915
335
1090
368
num-particulas
num-particulas
0
100
9
1
1
NIL
HORIZONTAL

MONITOR
1030
415
1145
460
Mejor Instrumentos
global-mejor-i
17
1
11

MONITOR
915
415
1030
460
Mejor Aleatoriedad
global-mejor-j
17
1
11

MONITOR
1145
415
1260
460
Mejor Intensidad
global-mejor-k
17
1
11

MONITOR
420
440
697
509
Diversion media del Público
(sum [diversion] of personas) / (count personas)
17
1
17

BUTTON
315
440
420
473
NIL
entrarPublico
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1095
160
1170
193
NIL
setupAvispas
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
915
160
970
193
Small
set stage \"stage1.jpg\"\nset afluencia 20\nset linea -24\nset size-personas 7\nsetup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
970
160
1025
193
Mid
set stage \"stage2.jpg\"\nset afluencia 100\nset linea -27\nset size-personas 3\nsetup\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
1225
160
1325
193
pso?
pso?
0
1
-1000

SWITCH
210
440
315
473
bailar?
bailar?
0
1
-1000

BUTTON
0
55
210
88
Probar percutiva
sound:play-drum percutiva-test intensidad
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
0
135
210
168
Probar melódica
sound:play-note melodica-test tonalidad (intensidad * 0.6) ((4 * 60 / bpm) / num-pasos)
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## WHAT IS IT?

El Hombre Orquesta tocará la mejor canción para el público en PSO

## HOW IT WORKS

Podemos mediante los controles jugar con el público y observar la mejora de la diversión de un público aleatorio con ciertas preferecias más o menos cuestionables sobre la música. El botón PSO determinará la mejor composición por nosotros mediante este algoritmo.

## HOW TO USE IT

1) Elige escenario
2) Pulsa go y mueve los parámetros
3) Pulsa setupAvispas y PSO y observa como se ajusta solo

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

+instrumentos
+shapes bailar
+secuenciador final
+menor carga en pso, pj particulas que no sean agentes


## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

by Rafa TM
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

ho0
false
0
Polygon -2064490 true false 195 120 225 210 240 210 210 120
Polygon -2064490 true false 90 120 60 165 45 180 60 195 75 165 105 120 105 105
Rectangle -2064490 true false 165 225 180 285
Rectangle -2064490 true false 120 225 135 285
Rectangle -7500403 true true 123 76 176 95
Polygon -1 true false 105 90 75 120 90 135 115 162 184 163 210 150 225 120 195 90
Polygon -13345367 true false 180 165 120 165 90 225 105 240 135 255 150 195 165 255 180 255 210 240
Circle -7500403 true true 110 5 80
Line -16777216 false 148 143 150 196
Rectangle -16777216 true false 116 186 182 198
Circle -1 true false 152 143 9
Circle -1 true false 152 166 9
Rectangle -16777216 true false 179 164 183 186
Polygon -955883 true false 180 90 195 90 195 165 195 195 150 195 165 165 180 90
Polygon -955883 true false 120 90 105 90 105 165 105 195 150 195 180 135 120 90
Rectangle -1184463 true false 180 54 195 60
Rectangle -16777216 true false 120 69 135 75
Rectangle -16777216 true false 135 174 150 180
Polygon -955883 true false 105 42 111 16 128 2 149 0 178 6 190 18 192 28 225 30 216 34 201 39 167 35
Line -16777216 false 120 45 135 45
Line -16777216 false 150 45 165 45
Polygon -16777216 true false 120 60 165 60
Line -16777216 false 120 60 165 60
Line -1184463 false 120 75 105 60
Line -1184463 false 105 60 105 45
Rectangle -16777216 true false 90 285 135 300
Rectangle -16777216 true false 165 285 210 300

ho1
false
0
Polygon -2064490 true false 210 135 285 150 285 135 210 120
Polygon -2064490 true false 90 120 60 165 45 180 60 195 75 165 105 120 105 105
Rectangle -2064490 true false 165 225 180 285
Rectangle -2064490 true false 120 195 135 255
Rectangle -7500403 true true 123 76 176 95
Polygon -1 true false 105 90 75 120 90 135 115 162 184 163 210 150 225 120 195 90
Polygon -13345367 true false 180 165 120 165 90 225 105 240 135 255 150 195 165 255 180 255 210 240
Circle -7500403 true true 110 5 80
Line -16777216 false 148 143 150 196
Rectangle -16777216 true false 116 186 182 198
Circle -1 true false 152 143 9
Circle -1 true false 152 166 9
Rectangle -16777216 true false 179 164 183 186
Polygon -955883 true false 180 90 195 90 195 165 195 195 150 195 165 165 180 90
Polygon -955883 true false 120 90 105 90 105 165 105 195 150 195 180 135 120 90
Rectangle -1184463 true false 180 54 195 60
Rectangle -16777216 true false 120 69 135 75
Rectangle -16777216 true false 135 174 150 180
Polygon -955883 true false 105 42 111 16 128 2 149 0 178 6 190 18 192 28 225 30 216 34 201 39 167 35
Line -16777216 false 120 45 135 45
Line -16777216 false 150 45 165 45
Polygon -16777216 true false 120 60 165 60
Line -16777216 false 120 60 165 60
Line -1184463 false 120 75 105 60
Line -1184463 false 105 60 105 45
Rectangle -16777216 true false 90 255 135 270
Rectangle -16777216 true false 165 285 210 300

ho2
false
0
Polygon -2064490 true false 195 120 225 210 240 210 210 120
Polygon -2064490 true false 90 120 45 120 15 120 15 135 75 135 105 120 105 105
Rectangle -2064490 true false 180 210 195 270
Rectangle -2064490 true false 120 225 135 285
Rectangle -7500403 true true 123 76 176 95
Polygon -1 true false 105 90 75 120 90 135 115 162 184 163 210 150 225 120 195 90
Polygon -13345367 true false 180 165 120 165 90 225 105 240 135 255 150 195 165 255 180 255 210 240
Circle -7500403 true true 110 5 80
Line -16777216 false 148 143 150 196
Rectangle -16777216 true false 116 186 182 198
Circle -1 true false 152 143 9
Circle -1 true false 152 166 9
Rectangle -16777216 true false 179 164 183 186
Polygon -955883 true false 180 90 195 90 195 165 195 195 150 195 165 165 180 90
Polygon -955883 true false 120 90 105 90 105 165 105 195 150 195 180 135 120 90
Rectangle -1184463 true false 180 54 195 60
Rectangle -16777216 true false 120 69 135 75
Rectangle -16777216 true false 135 174 150 180
Polygon -955883 true false 105 42 111 16 128 2 149 0 178 6 190 18 192 28 225 30 216 34 201 39 167 35
Line -16777216 false 120 45 135 45
Line -16777216 false 150 45 165 45
Polygon -16777216 true false 120 60 165 60
Line -16777216 false 135 75 165 60
Line -1184463 false 120 75 105 60
Line -1184463 false 105 60 105 45
Rectangle -16777216 true false 90 285 135 300
Rectangle -16777216 true false 180 270 225 285

ho3
false
0
Polygon -2064490 true false 75 120 105 210 120 210 90 120
Polygon -2064490 true false 210 135 180 180 165 195 180 210 195 180 225 135 225 120
Rectangle -2064490 true false 180 225 195 285
Rectangle -2064490 true false 90 225 105 285
Rectangle -7500403 true true 123 76 176 95
Polygon -1 true false 105 90 75 120 90 135 115 162 184 163 210 150 225 120 195 90
Polygon -13345367 true false 180 165 120 165 90 225 105 240 135 255 150 195 165 255 180 255 210 240
Circle -7500403 true true 110 5 80
Line -16777216 false 148 143 150 196
Rectangle -16777216 true false 116 186 182 198
Circle -1 true false 152 143 9
Circle -1 true false 152 166 9
Rectangle -16777216 true false 179 164 183 186
Polygon -955883 true false 180 90 195 90 195 165 195 195 150 195 165 165 180 90
Polygon -955883 true false 120 90 105 90 105 165 105 195 150 195 180 135 120 90
Rectangle -1184463 true false 180 54 195 60
Rectangle -16777216 true false 120 69 135 75
Rectangle -16777216 true false 135 174 150 180
Polygon -955883 true false 105 42 111 16 128 2 149 0 178 6 190 18 192 28 225 30 216 34 201 39 167 35
Line -16777216 false 120 45 135 45
Line -16777216 false 150 45 165 45
Polygon -16777216 true false 120 60 165 60
Line -16777216 false 120 60 135 60
Line -1184463 false 120 75 105 60
Line -1184463 false 105 60 105 45
Rectangle -16777216 true false 60 285 105 300
Rectangle -16777216 true false 180 285 225 300

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

note
false
0
Circle -7500403 true true 90 180 90
Polygon -7500403 true true 165 195 165 45 210 30 270 60 240 75 210 45 180 60 180 225 165 195
Polygon -7500403 true true 180 90 210 75 270 120 240 135 210 90 165 120

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 195 90 180 195 210 285 195 300 165 300 150 225 135 300 105 300 90 285 120 195 105 90
Rectangle -7500403 true true 135 75 165 94
Polygon -7500403 true true 105 90 60 150 75 180 135 105
Polygon -7500403 true true 195 90 240 150 225 180 165 105

personacani
false
0
Circle -7500403 true true 116 15 68
Polygon -7500403 true true 180 90 180 190 191 244 168 247 156 243 151 202 146 240 130 250 110 243 124 186 120 90
Rectangle -7500403 true true 135 75 165 94
Polygon -7500403 true true 120 90 79 173 90 180 135 105
Polygon -7500403 true true 180 90 220 172 210 180 165 105
Polygon -7500403 true true 117 33 102 18 117 18 117 3 132 18 132 3 147 18 147 3 162 18 162 3 177 18 192 3 192 33 207 48 177 63 117 33
Polygon -7500403 true true 120 240 120 285 105 300 120 300 135 285 135 240 120 240
Polygon -7500403 true true 165 240 165 285 180 300 195 300 180 285 180 240

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tile brick
false
0
Rectangle -1 true false 0 0 300 300
Rectangle -7500403 true true 15 225 150 285
Rectangle -7500403 true true 165 225 300 285
Rectangle -7500403 true true 75 150 210 210
Rectangle -7500403 true true 0 150 60 210
Rectangle -7500403 true true 225 150 300 210
Rectangle -7500403 true true 165 75 300 135
Rectangle -7500403 true true 15 75 150 135
Rectangle -7500403 true true 0 0 60 60
Rectangle -7500403 true true 225 0 300 60
Rectangle -7500403 true true 75 0 210 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="1" runMetricsEveryStep="true">
    <setup>prueba</setup>
    <go>play</go>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="hi-hat">
      <value value="&quot;Closed Hi Hat&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bass-drum?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bass-drum">
      <value value="&quot;Bass Drum 1&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bass?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tom">
      <value value="&quot;Hi Timbale&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="snare?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="percussion">
      <value value="&quot;Cowbell&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hi-hat?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="snare">
      <value value="&quot;Acoustic Snare&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tom?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="percussion?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bpm">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cymbal?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cymbal">
      <value value="&quot;Crash Cymbal 1&quot;"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
1
@#$#@#$#@
