extensions [sound]

breed [coches coche]
breed [interfaces interfaz]
breed [checkpoints checkpoint]
breed [radares radar]
radares-own [activacion id nombre]

;;Redes neuronales
directed-link-breed [conexiones-sinapticas conexion-sinaptica]
directed-link-breed [conexiones-scanner conexion-scanner]
conexiones-sinapticas-own [peso]
breed [nodos-preprocesado nodo-preprocesado]
nodos-preprocesado-own [activacion dev p2 id]
breed [nodos-entrada nodo-entrada]
nodos-entrada-own [activacion dev p2 id]
breed [nodos-salida nodo-salida]
nodos-salida-own [activacion dev p2 id nombre]
breed [nodos-ocultos nodo-oculto]
nodos-ocultos-own [activacion dev p2 id]

;; Aprendizaje
breed [vertices vertice] ;; ID3
vertices-own [id]
directed-link-breed [aristas arista]
breed [acciones accion]
acciones-own [do]
directed-link-breed [transiciones transicion]
transiciones-own [on Q]

patches-own [
  area 
  color-original 
  marcado?
]

globals [
  registro       ; Lista de patches del scanner del coche, que irá a la entrada de la red
  data-list      ; Lista de pares [Entrada Salida] para entrenar el sistema
  color-pista    ; Color que aceptará la red como carretera (el resto será negro)
  entradas       ; Lista que contendrá cada una de las entradas individuales en el entrenamiento (binario)
  salidas        ; Lista que contendrá cada una de las salidas individuales en el entrenamiento (binario)
  epoch-error    ; Error que se comete en cada iteración del entrenamiento
  t              ; Instante de tiempo guardado
  objetivo       ; Punto a donde debe ir IT sin salirse del camino
  dataset        ; Datos de acciones-sensores para ID3
  posibles-acciones ; lista de las posibilidades implementadas
  final          ; acción ejecutada finalmente
]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup
  ct
  reset-timer
  setup-map
  setup-areas
  setup-nodos
  setup-conexiones-sinapticas
  setup-acciones
  setup-vars
  recolor
  setup-it
  setup-radar
  setup-transiciones
  if intro? = true [intro]
end

to go
  ;; Si s ete ha pasado testear la pista
  if color-pista = 0 [terreno]
  ask coches [
    
    ; Registra el suelo y lo manda a las entradas de la red neuronal
    scanner
    
    ;; La red neuronal identifica el camino
    test-neuronal
    
    ; Utiliza el radar -> checkpoint (N, S, E, O)
    test-radar
    
    ; IT decide la mejor acción BASIC vs ID3
    let accion []
    
    ;temp id3
    ;let aux decide-id3
    
    if-else id3-on? = true [set accion decide-id3] [set accion decide-basic]
    ;print accion
    
    ;; IT ejecuta la acción
    ; IA on / off 
    if piloto-auto? = true [ ejecuta accion ]
    
    ;; Aprendizaje
    ;; Aprende siempre lo mismo
    ;; no he encontrado un sistema de estados y acciones en el tiempo 
    ;; que funcione con Q-learning en este problema, que era mi intención al principio
    ;aprende-basic
    
    ask checkpoints in-radius getCor 5 [die]
    
    ; EN MOVIMIENTO!
    set heading girar fd velocidad
    if rec? = true [grabar-decisiones]
  ]
  
end 

to setup-it
  create-coches 1[ 
    move-to patch getCor 125 getCor 10 
    set size getCor 20 
    set shape "it"
    set heading -90]
  reset-ticks
end

to setup-map
  restablecer-areas
  ;; Areas del mapa
  ask patches [set area ""]
  ask patches with [pxcor < getCor 65] [set area "Marcador"]
  
  ask patches with [pxcor > getCor 1 and pycor > getCor 1 and pycor < getCor 28 and area = "Marcador"] [set area "ID3" set pcolor 1]
  
  ;;ask patches with [(pycor < -1.5 * pxcor + 130) and (pycor > -1.5 * pxcor + 65)] [set area "pista"]
  ;;ask patches with [pxcor > 50 and pycor > 10 and pxcor < 268 and pycor < 45] [set area "pista"]
  
  ;Zona de nodos de preprocesado
  if-else HD? = false [ask patches with [pxcor > 6 and pxcor < 14 and pycor < (max-pycor - 10) and pycor > (max-pycor - 18)] [set pcolor black set area "nodos-preprocesado"]]
                      [ask patches with [pxcor > 12 and pxcor < 27 and (pxcor mod 2) = 1 and pycor < (max-pycor - 10) and pycor > (max-pycor - 25) and (pycor mod 2) = 1] [set pcolor black set area "nodos-preprocesado"]]
  ;Zona de nodos de entrada
  if-else HD? = false [ask patches with [pxcor > 20 and pxcor < 28 and pycor < (max-pycor - 10) and pycor > (max-pycor - 18)] [set pcolor blue set area "nodos-entrada"]]
                      [ask patches with [pxcor > 40 and pxcor < 55 and (pxcor mod 2) = 1 and pycor < (max-pycor - 10) and pycor > (max-pycor - 25) and (pycor mod 2) = 1] [set pcolor blue set area "nodos-entrada"]]
  ;Zona de nodos de la capa oculta
  ;;ask patches with [pxcor > 34 and pxcor < 48 and pycor = (max-pycor - 7)] [set pcolor pink set area "nodos-ocultos"]
  let i 0
  let x 0 let y 0
  if-else HD? = false [set x min-pxcor + 60 set y max-pycor - 25][set x min-pxcor + 120 set y max-pycor - 45]
  repeat nodos-capa-oculta [
    ask patch x (y + i) [set pcolor pink set area "nodos-ocultos"]
    if-else HD? = false [set i i + 1][set i i + 2]
    ]
  ;Zona de nodos de salida
  if-else HD? = false [ask patches with [pxcor >= 10 and pxcor < 51 and pxcor mod 10 = 0 and pycor = (max-pycor - 30)] [set pcolor yellow set area "nodos-salida"]]
                      [ask patches with [pxcor >= 20 and pxcor < 101 and pxcor mod 20 = 0 and pycor = (max-pycor - 60)] [set pcolor yellow set area "nodos-salida"]]
  ; Zona de acciones
  if-else HD? = false [ask patches with [pxcor >= 8 and pxcor < 57 and pxcor mod 8 = 0 and pycor = (max-pycor - 45)] [set pcolor yellow set area "acciones"]]
                      [ask patches with [pxcor >= 16 and pxcor < 114 and pxcor mod 16 = 0 and pycor = (max-pycor - 90)] [set pcolor yellow set area "acciones"]]
  
  ;;Ajuste de patches
  ;while [any? patches with [area = ""]] [ask patches with [area = ""] [set area [area] of one-of neighbors]]
  ;
end

to setup-vars
  ;; RED
  set epoch-error 0
  clear-plot
  reset-ticks
  ask conexiones-sinapticas [set peso random-float 0.2 - 0.1]
  set data-list []
  set color-pista 0
  set posibles-acciones (list "girar-i" "girar-d" "0" "1ra" "2da" "3ra" "R" )
end

; Crea los nodos de la red neuronal
to setup-nodos
  
  ;DATOS MODIFICABLES
  let num-nodos-ocultos 5
  let tamano 10
  
  set-default-shape nodos-preprocesado "square"
  set-default-shape nodos-entrada "square"
  set-default-shape nodos-salida "circle"
  set-default-shape nodos-ocultos "circle 2"
  
  ; Creación de las unidades de proprocesado
  let i 0
  foreach sort patches with [area = "nodos-preprocesado"]
  [ask ? [ 
      sprout-nodos-preprocesado 1 [set id i]]
      set i i + 1
  ]
  ask one-of nodos-preprocesado with [id = 0] [set label "Scan"]
  
  ; Creación de las neuronas entradas
  set i 0
  foreach sort patches with [area = "nodos-entrada"]
  [ask ? [ 
      sprout-nodos-entrada 1 [ set activacion random-float 0.1 set id i]]
      set i i + 1
  ]
  ask one-of nodos-entrada with [id = 0] [set label "Preproc"]
  
  ; Creación de neuronas de la capa intermedia
  set i 0
  foreach sort patches with [area = "nodos-ocultos"] 
  [ask ? [
    sprout-nodos-ocultos 1 [ set activacion random-float 0.1 set id i]]
      set i i + 1
  ]
  ; Creación de las neuronas de salida
  set i 0
  let formas (list "curva-i" "curva-d" "cortado" "leve" "recta")
  foreach sort patches with [area = "nodos-salida"] 
  [ask ? [
    sprout-nodos-salida 1 [ 
      set activacion random-float 0.1 
      set id i set size (28 / patch-size) 
      set shape item i formas 
      set color white 
      set label item i formas
      set nombre item i formas 
      set label-color black]]
   set i i + 1
  ]
end

to setup-conexiones-sinapticas
  ask nodos-preprocesado [create-conexiones-scanner-to nodos-entrada with [id = [id] of myself]]
  conecta nodos-entrada nodos-ocultos
  conecta nodos-ocultos nodos-salida
end

to setup-radar
  let x getCor 32 
  let y getCor 40
  let real one-of coches 
  create-radares 1 [ 
    set shape "radar" set color grey set size 0.5 * [size] of real set id 0
    move-to patch (x - getCor 5) y
    set heading 180
    set nombre "O"
    set label nombre
  ]
  create-radares 1 [ 
    set shape "radar" set color grey set size 0.5 * [size] of real set id 1
    move-to patch x (y + getCor 5)
    set heading 270
    set nombre "N"
    set label nombre
  ]
  create-radares 1 [ 
    set shape "radar" set color grey set size 0.5 * [size] of real set id 2
    move-to patch (x + getCor 5) y
    set heading 0
    set nombre "E"
    set label nombre
  ]
  create-radares 1 [ 
    set shape "radar" set color grey set size 0.5 * [size] of real set id 3
    move-to patch x (y -  getCor 5)
    set heading 90
    set nombre "S"
    set label nombre
  ]
  create-interfaces 1 [
    set shape [shape] of real 
    set size 1 * [size] of real 
    set color [color] of real
    set heading 0 
    move-to patch x y 
    stamp
    die]
end

to setup-acciones
  let i 0
  let formas (list "girar-i" "girar-d" "0" "1ra" "2da" "3ra" "R" )
  foreach sort patches with [area = "acciones"] 
  [ask ? [
    sprout-acciones 1 [ 
      set size (28 / patch-size) 
      set shape item i formas 
      set color white 
      set label item i formas 
      set label-color black]]
   set i i + 1
  ]
end

to setup-transiciones
  ask nodos-salida [
    create-transiciones-to acciones [set Q 0]  
  ]
  ask radares [
    create-transiciones-to acciones [set Q 0]  
  ]
end

to setup-checkpoints
  ask checkpoints [die]
  let checks (list patch getCor 77 getCor 22 patch getCor 95 getCor 85 patch getCor 190 getCor 90 patch getCor 120 getCor 70 patch getCor 185 getCor 30)
  foreach checks [
    ask ? [sprout-checkpoints 1 [ set shape "flag" set size getCor 5 set color yellow]]
  ]
end

to setup-areas
  let x getCor 140
  let y getCor 13
  ask patch x y [ask patches in-radius getCor 6 [set area "Meta"]]
end

to conecta [nodos1 nodos2]
  ask nodos1 [
    create-conexiones-sinapticas-to nodos2 [
      set peso random-float 0.2 - 0.1
    ]
  ]
end

to pista
  cp
  if-else HD? = true [set-patch-size 2 resize-world 0 400 0 200] [set-patch-size 4 resize-world 0 200 0 100]
  import-pcolors (word "mapas/" mapa ".jpg")
  ask patches [set color-original pcolor]
  setup-areas
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RED NEURONAL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;
;;; Procedimiento de entrenamiento
;;;

to entrenar
 
  ; Repetimos mientras se quiera
    tick
    ; Para cada dato de entrenamiento
    foreach data-list [
      ; Recuperamos la entrada y salida deseada
      set entradas first ?
      set salidas last ?
      ; Cargamos la entrada en los nodos de entrada
      (foreach (sort nodos-entrada) entradas [
        ask ?1 [set activacion ?2]])
      ; Propagamos la señal
      propaga
      ; Propagamos hacia atrás el error respecto de la salida esperada
      back-propaga
    ]
    plotxy ticks epoch-error ;;plot the error
    set epoch-error (epoch-error / ticks) 
end

to olvidar
  ;; RED
  set epoch-error 0
  clear-plot
  reset-ticks
  ask conexiones-sinapticas [set peso random-float 0.2 - 0.1]
end

;;;
;;; Procedimientos de Propagación
;;;

;; Propagación de la señal a lo largo de la red
to propaga
  ask nodos-ocultos [ set activacion calcula-activacion ]
  ask nodos-salida  [ set activacion calcula-activacion ask my-out-transiciones [set on [activacion] of myself]]
  recolor
end

to-report calcula-activacion
  report sigmoide sum [[activacion] of end1 * peso] of my-in-conexiones-sinapticas
end

;; Propagación hacia atrás del error cometido en las salidas
to back-propaga
  let error-ejemplo 0
  ; Se calcula el error de las salidas
  (foreach (sort nodos-salida) salidas [
    ask ?1 [ set dev activacion * (1 - activacion) * (?2 - activacion) ]
    set error-ejemplo error-ejemplo + ( (?2 - [activacion] of ?1) ^ 2 )])
  ; Se acumula el error medio de las salidas en el error del epoch actual
  set epoch-error epoch-error + (error-ejemplo / count nodos-salida)
  ; Se calcula el error en los nodos de la capa oculta
  ask nodos-ocultos [
    set dev activacion * (1 - activacion) * sum [peso * [dev] of end2] of my-out-conexiones-sinapticas
  ]
  ; Se actualizan los pesos de las aristas
  ask conexiones-sinapticas [
    set peso peso + tasa-aprendizaje * [dev] of end2 * [activacion] of end1
  ]
  set epoch-error epoch-error / 2
end

;; Ejecuta la fución de aproximación  
to test-neuronal
  let patron map [[color] of ?] (sort nodos-entrada) 
  set entradas map [ifelse-value (? = black) [1] [0]] patron
  activar-entradas
  propaga
end

; Activamos los nodos de entrada en función de las entradas leidas
to activar-entradas
(foreach entradas (sort nodos-entrada )
  [ask ?2 [set activacion ?1]])
  recolor
end

;; Almacena el par [entradas salidas] en los datos de entrenamiento
to almacena
  if salida = "curva-i" [set salidas (list 1 0 0 0 0)]
  if salida = "curva-d" [set salidas (list 0 1 0 0 0)]
  if salida = "cortado" [set salidas (list 0 0 1 0 0)]
  if salida = "leve" [set salidas (list 0 0 0 1 0 )]
  if salida = "recta" [set salidas (list 0 0 0 0 1)]
  set data-list (lput (list entradas salidas) data-list)
end

to terreno
  ask coches [set color-pista (((int (pcolor / 10)) * 10) + 5)]
end

to cargar [f]
  ;setup
  file-open f
  ;carefully [ uno ] [ si falla ]
  set data-list read-from-string file-read-line
  file-close-all
end

to guardar [f]
  carefully [file-delete f][]
  file-open f
  file-write data-list
  file-close-all
end

to save [f]
  carefully [file-delete f][]
  file-open f
  let aux output-dataset dataset
  file-write aux
  file-close-all
end

;; Recolorea
to recolor
  ask nodos-entrada [
    set color item (step activacion) [white black]
  ]
  ask nodos-salida [
    set color item (step activacion) [white green]
  ]
  ask transiciones [
    set color item (step on) [white green]
  ]
  let MaxP max [abs peso] of conexiones-sinapticas
  ask conexiones-sinapticas [
    set thickness 0.1 * abs peso
    ifelse peso > 0
      [ set color lput (255 * abs peso / MaxP) [0 0 255]]
      [ set color lput (255 * abs peso / MaxP) [255 0 0]]
  ]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PROCEDIMIENTOS DE TOMA DE DECICIONES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to aprende-basic
  ;; Q estáticos
  ask acciones with [label = "girar-i"] [
    ask my-in-transiciones with [[nombre] of end1 = "curva-i"] [set Q 1]
    ask my-in-transiciones with [[nombre] of end1 = "recta"] [set Q .5]
    ask my-in-transiciones with [[nombre] of end1 = "O"] [set Q .5]
  ]
  ask acciones with [label = "girar-d"] [
    ask my-in-transiciones with [[nombre] of end1 = "curva-d"] [set Q 1]
    ask my-in-transiciones with [[nombre] of end1 = "recta"] [set Q .5]
    ask my-in-transiciones with [[nombre] of end1 = "E"] [set Q .5]
  ]
  ask acciones with [label = "0"] [
    ask my-in-transiciones with [[nombre] of end1 = "cortado"] [set Q .5]
    ask my-in-transiciones with [[nombre] of end1 = "S"] [set Q .5]
  ]
  ask acciones with [label = "1ra"] [
    ask my-in-transiciones with [[nombre] of end1 = "leve"] [set Q 1]
  ]
  ask acciones with [label = "2da"] [
    ask my-in-transiciones with [[nombre] of end1 = "recta"] [set Q 1]
  ]
  ask acciones with [label = "3ra"] [
    ask my-in-transiciones with [[nombre] of end1 = "recta"] [set Q .4]
    ask my-in-transiciones with [[nombre] of end1 = "N"] [set Q .4]
  ]
  ask acciones with [label = "R"] [
    ask my-in-transiciones with [[nombre] of end1 = "cortado"] [set Q 1]
    ask my-in-transiciones with [[nombre] of end1 = "recta"] [set Q .5]
    ask my-in-transiciones with [[nombre] of end1 = "S"] [set Q .5]
  ]
  refuerza
end

to-report decide-basic
  ask acciones [
    set do step totalQ
    set color item do [white yellow]]
  let res []
  let aux (list "girar-i" "girar-d" "0" "1ra" "2da" "3ra" "R" )
  foreach aux [
    if [do] of one-of acciones with [label = ?] = 1 [set res lput ? res]
    ]
  
  aprende-basic
  report res
end

to refuerza
  ; Normalizamos la bondad de las transiciones
  ; y las reflejamos en color y anchura de las aristas
  let MaxQ max [Q] of transiciones
  ask transiciones [
    set Q Q / MaxQ
    ;set color scale-color color Q 1 0
    set thickness getCor 0.5 * Q
  ]
end

to-report totalQ
  report sum [Q] of my-in-transiciones with [step on = 1]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ID3
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to-report decide-id3
  let todos (turtle-set nodos-salida radares)
  ;let activos vertices  with [shape != "square" or label != "accion" or member? label posibles-acciones = true]
  ;print activos
  ask vertices with [shape != "square" and label != "accion"] [
    set size getCor 2
    ;print [label] of todos
    ;print label
    if todos != nobody [
      if-else [step activacion] of one-of todos with [label = [label] of myself] = 1 [
        set size getCor 4
        ask my-out-aristas [ if-else label = "on" [ set thickness 2] [set thickness 0.1]]
        ][
        set size getCor 2
        ask my-out-aristas [ if-else label = "off" [ set thickness 2] [set thickness 0.1]]
        ]        
      ]
  ]
  
  let res [label] of vertices with [shape = "square" and count my-in-links with [thickness = 2] > 0]
  ask acciones [
    set do 0
    if member? label res = true [set do 1]
    set color item do [white yellow]
  ]
  
  report res
end

to grabar-decisiones
  let outs sort nodos-salida
  let linea []
  foreach outs [
    set linea lput (item ([step activacion] of ?) ["off" "on"]) linea
    ]
  set linea lput (item ([step activacion] of one-of radares with [label = "N"]) ["off" "on"]) linea
  set linea lput (item ([step activacion] of one-of radares with [label = "S"]) ["off" "on"]) linea
  set linea lput (item ([step activacion] of one-of radares with [label = "E"]) ["off" "on"]) linea
  set linea lput (item ([step activacion] of one-of radares with [label = "O"]) ["off" "on"]) linea
  set linea lput final linea
  ;print linea
  if member? linea dataset = false [
    set dataset lput linea dataset 
    clear-output
    output-print output-dataset dataset
  ]
end

; Procedimiento principal q genera el árbol de decisión obtenido por ID3
to ID3-main 
  ask vertices [die]
  if-else dataset != 0 [
  create-vertices 1 [ 
    set id 0
    set size getCor 2 ;;;;;;;;
    set shape "circle"
    set color red
    set label (last first dataset)
    move-to one-of patches with [area = "ID3"]
    create-arista-to ID3 dataset
  ;layout-radial vertices aristas (one-of vertices with [id = 0])
  clear-output
  output-print output-dataset dataset
  ]][
  clear-output
  output-print "No hay datos"
  ]
end

; Algoritmo ID3 recursivo.
;  Diferencia entre un vertice-hoja (lo que queda ya está clasificado)
;                 o un vertice-decisión (hay que clasificar más)
;  Devuelve siempre el vertice correspondiente, y si hay que continuar
;   con la clasificación, entonces genera las ramas correspondientes
;   y llama recursivamente al procedimiento con los nuevos datasets.
to-report ID3 [ds]
  let r nobody
  ifelse clasificado? ds
  [
    hatch-vertices 1
    [
      set id count vertices + 1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      set shape "square"
      set color red
      set size getCor 2
      set r self
      set label (word last last ds)
      move-to one-of patches with [area = "ID3"]
    ]
  ]
  [
    let mx 0
    ifelse Max.Metodo = "Ganancia Información"
    [set mx Max-GI ds]
    [set mx Max-GR ds]
    hatch-vertices 1
    [
      set id count vertices + 1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      set label mx
      set r self
      set color blue
      set size getCor 3
      set shape "decision"
      ;setxy random-pxcor random-pycor
      move-to one-of patches with [area = "ID3"]
      foreach (remove-duplicates bf (columna mx ds))
      [
        create-arista-to (ID3 filtra ds mx ?) 
        [
          set color green 
          set label ?
          set label-color green + 3
        ]
      ]
    ] 
  ]
  report r
end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNCIONES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Función sigmoide
to-report sigmoide [x]
  report 1 / (1 + e ^ (- x))
end

;; Función step 
to-report step [x]
  ifelse x > 0.5
    [ report 1 ]
    [ report 0 ]
end

; Devuelve el subdataset que se obtiene filtrando uno de sus atributos por un valor
to-report filtra [ds atr val]
  let atrs first ds
  let p position atr atrs
  let ds2 (fput (first ds) filter [(item p ?) = val] (bf ds))
  report ds2
end

; Devuelve la entropia de una lista
to-report entropia [l]
  ; Calculamos la frecuencia relativa de cada uno de sus elementos
  let l2 map [frec ? l] (remove-duplicates l)
  ; Aplicamos la función de entropía a la lista de frecuencias relativas
  report sum map [ifelse-value (? = 0)[0][(-1) * ? * log ? 2]] l2 
end

; Devuelve la entropía relativa de un atributo en un dataset
to-report entropia-rel [ds atr]
  ; Extraemos la lista de atributos del dataset
  let atrs first ds
  ; Extraemos los posibles valores del atributo
  let val  bf (columna atr ds)
  ;show val
  ; Nº de columna asociada al atributo
  let p position atr atrs
  ;show p
  ; Acumulador para calcular la entropia relativa
  let s 0
  ; Para cada posible valor del atributo:
  foreach remove-duplicates val
  [
    let v ?
    ; Calculamos su frecuencia relativa
    let f frec v val
    ; Filtramos solo aquellas filas que tienen ese valor en el atributo, y 
    ; formamos un nuevo dataset, con cabecera, con esas filas
    let ds2 (fput (first ds) filter [(item p ?) = v] (bf ds))
    ;show-dataset ds2
    ; Calculamos la entropia de la última columna en el dataset filtrado
    let ex entropia  bf (columna (last atrs) ds2)
    ; Lo acumulamos
    set s s + f * ex
  ]
  report s
end

to-report GI [ds atr]
  let atrs first ds
  report (entropia bf (columna (last atrs) ds)) - (entropia-rel ds atr)
end

; Devuelve la frecuencia relativa de un elemento en una lista
to-report frec [el lista]
  report (length (filter [? = el] lista)) / (length lista)
end

; Devuelve de un dataset la columna correspondiente a uno de sus atributos. 
; Importante: CON LA CABECERA
to-report columna [at ds]
  let ats first ds
  let p position at ats
  report map [item p ?] ds
end

to muestra-patch [p]
  print (word [pxcor] of p " - " [pycor] of p)  
end

to muestra-list-patches [l]
  foreach l [
  print (word "(" [pxcor] of ? "," [pycor] of ? ")")  ]
end

;; Funciones de dibujo
;; Marca el area seleccioada para visualizarla
to marcar-area [nombre-area]
  let temp-color one-of base-colors
  ask patches with [area = nombre-area] [set pcolor temp-color set marcado? true]
end

;; Reestablece las areas a su color original
to restablecer-areas
  ask patches with [marcado? = true] [set pcolor color-original]
end


;; Procedimiento de patches
;; Devuelve el color medio de donde se encuentra
to-report media-color
  report sum [pcolor] of neighbors / count neighbors
end

to-report velocidad
  report (pedal * marcha) / cpu
end

;; EJECUTA
to ejecuta [lista-acciones]
  set girar heading ; por si el usuario ha tocado los controles
  foreach lista-acciones [
    set final ?
    if ? = "girar-i" [set girar girar - 1]
    if ? = "girar-d" [set girar girar + 1]
    if ? = "R" [set marcha -1]
    if ? = "0" [set marcha 0]
    if ? = "1ra" [set marcha 1]
    if ? = "2da" [set marcha 2]
    if ? = "3ra" [set marcha 3]
  ]
end

to test-radar
  set objetivo min-one-of checkpoints [who]
  sensores objetivo
  ; El radar envia su señal
  ask radares [ask my-out-transiciones [set on [activacion] of myself]]
end

;;RADAR
to sensores [x]
  ask radares [set color grey set activacion 0]
  ask checkpoints [face myself] 
    if x != nobody [
      let dif subtract-headings heading [heading] of x
      ;S
      if dif > -15 and dif <= 15 [ask radares with [heading = 90] [set color green set activacion 1] ]
      ;E
      ;if dif > 45 and dif <= 135 [ask radares with [heading = 0] [set color green set activacion 1] ]
      ;Mejor si E y O abarcan 180 grados
      if dif > 0 [ask radares with [heading = 0] [set color green set activacion 1] ]
      ;N
      if (dif > 165 and dif <= 180) or (dif >= -180 and dif <= -165) [ask radares with [heading = 270] [set color green set activacion 1] ]
      ;0
      ;if dif > -135 and dif <= -45 [ask radares with [heading = 180] [set color green set activacion 1] ]
      if dif < 0 [ask radares with [heading = 180] [set color green set activacion 1] ]
    ]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Registra el suelo y lo manda a la red neuronal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to scanner
  
  ;fila 0
  set registro (list 
      (patch-left-and-ahead 26.5 (6 / cos 26.5)) 
      (patch-left-and-ahead 18.5 (6 / cos 18.5))
      (patch-left-and-ahead 10.5 (6 / cos 10.5))
      (patch-ahead 6)
      (patch-right-and-ahead 10.5 (6 / cos 10.5))
      (patch-right-and-ahead 18.5 (6 / cos 18.5))
      (patch-right-and-ahead 26.5 (6 / cos 26.5))
  
  ;fila 1
      (patch-left-and-ahead 31.5 (5 / cos 31.5))
      (patch-left-and-ahead 21.5 (5 / cos 21.5))
      (patch-left-and-ahead 11.5 (5 / cos 11.5))
      (patch-ahead 5)
      (patch-right-and-ahead 11.5 (5 / cos 11.5))
      (patch-right-and-ahead 21.5 (5 / cos 21.5))
      (patch-right-and-ahead 31.5 (5 / cos 31.5))
  
  ;fila 2
      (patch-left-and-ahead 37 (4 / cos 37))
      (patch-left-and-ahead 26.5 (4 / cos 26.5))
      (patch-left-and-ahead 15 (4 / cos 15))
      (patch-ahead 4)
      (patch-right-and-ahead 15 (4 / cos 15))
      (patch-right-and-ahead 26.5 (4 / cos 26.5))
      (patch-right-and-ahead 37 (4 / cos 37))
  
  ;fila 3
      (patch-left-and-ahead 45 (3 / cos 45))
      (patch-left-and-ahead 30 (3 / cos 30))
      (patch-left-and-ahead 15 (3 / cos 15))
      (patch-ahead 3)
      (patch-right-and-ahead 15 (3 / cos 15))
      (patch-right-and-ahead 30 (3 / cos 30))
      (patch-right-and-ahead 45 (3 / cos 45))
 
  ;fila 4
      (patch-left-and-ahead 55 (2 / cos 55))
      (patch-left-and-ahead 40 (2 / cos 40))
      (patch-left-and-ahead 25 (2 / cos 25))
      (patch-ahead 2)
      (patch-right-and-ahead 25 (2 / cos 25))
      (patch-right-and-ahead 40 (2 / cos 40))
      (patch-right-and-ahead 55 (2 / cos 55))
  
  ;fila 5
      (patch-left-and-ahead 71.5 (1 / cos 71.5))
      (patch-left-and-ahead 62.5 (1 / cos 62.5))
      (patch-left-and-ahead 45 (1 / cos 45))
      (patch-ahead 1)
      (patch-right-and-ahead 45 (1 / cos 45))
      (patch-right-and-ahead 62.5 (1 / cos 62.5))
      (patch-right-and-ahead 71.5 (1 / cos 71.5))
  
  ;fila 6
      (patch-left-and-ahead 90 3)
      (patch-left-and-ahead 90 2)
      (patch-left-and-ahead 90 1)
      (patch-here)
      (patch-right-and-ahead 90 1)
      (patch-right-and-ahead 90 2)
      (patch-right-and-ahead 90 3)
  )
  
  let i 0
  let primero [who] of one-of nodos-preprocesado with [id = 0]
  foreach registro [ 
    if ? != nobody [ask nodo-preprocesado (primero + i) [set color [pcolor] of ?]]
    set i i + 1
    ]
  
  ask nodos-entrada [
    let temp-color [color] of [end1] of one-of my-in-conexiones-scanner
    if color-pista != 0 [if-else temp-color > (color-pista - 4) and temp-color < (color-pista + 4) [set color white] [set color black]]
    ]
   
end

;;INTRO quickstart
to intro
  ask coches [ 
    wait 1 
    set label "HOLA" 
    wait 2 
    set label "" 
    wait 1 
    set label "Usa los controles para manejar el coche" 
    wait 3 
    set label "" 
    wait 1 
    set label "En pista firme, pulsa terreno para que el scanner pueda funcionar"
    wait 4 
    set label "" 
    wait 1 
    set label "Conduce hasta giros, rectas, etc y seleccionando la opción correcta pulsa almacenar."
    wait 5 
    set label "" 
    wait 1 
    set label "También puedes cargar uno de los ficheros de entrenamiento"
    wait 4 
    set label "" 
    wait 1 
    set label "Despúes de entrenar la red puedes usar el piloto automático"
    wait 4
    set label ""
 ]
end

; Muestra un dataset de forma ordenada
to show-dataset [ds]
  let m 1 + max map [max map [length (word ?)] ?] ds
  output-print "Dataset:"
  repeat 1 + (m + 1) * length (first ds) [output-type "-"]
  output-print ""
  foreach ds
  [
    output-type "|"
    foreach ?
    [
      output-type ? 
      repeat (m - length (word ?)) [output-type " "]
      output-type "|"
    ]
    output-print ""
    repeat 1 + (m + 1) * length (first ds) [output-type "-"]
    output-print ""
  ]
end

to-report output-dataset [ds]
  let s "Dataset:\n"
  let atrs first ds
  let long map [1 + max map [length (word ?)] (columna ? ds)] atrs
  set s (word s "\n")
  ;; Lineas
  let lin "├"
  foreach long
  [
    repeat (1 + ?) [set lin (word lin "─")]
    set lin (word lin "┼")
  ]
  set lin (word (bl lin) "┤\n")
  ;; Cabecera
  set s (word s "│ ")
  (foreach (first ds) long
    [
      set s (word s  ?1 )
      repeat (?2 - length (word ?1)) [set s (word s " ")]
      set s (word s "│ ")
    ])
  set s (word s "\n" lin)
  ; Cuerpo
  foreach bf ds
  [
    set s (word s lin)
    set s (word s "│ ")
    (foreach ? long
    [
      set s (word s  ?1 )
      repeat (?2 - length (word ?1)) [set s (word s " ")]
      set s (word s "│ ")
    ])
    set s (word s "\n")
    ;set s (word s lin)
  ]
  ;; Linea final
  let lin2 "└"
  foreach long
  [
    repeat (1 + ?) [set lin2 (word lin2 "─")]
    set lin2 (word lin2 "┴")
  ]
  set lin2 (word (bl lin2) "┘\n")
  set s (word s lin2)
  ; Devolución
  report s
end

; Carga un Dataset desde un fichero
to-report load-Dataset
  ;ca
  file-close-all
  let f user-file
  ;let f "Test3-ID3.txt"
  ifelse is-string? f
  [
    set dataset []
    file-open f
    while [not file-at-end?]
    [
      set dataset lput (read-from-string (word "[" file-read-line "]")) dataset
    ]
    file-close
    report true
  ]
  [report false]
end

; Devuelve el atributo de máxima ganancia de un dataset
to-report Max-GI [ds]
  let atr1 map [(list ? (GI ds ?) )] (bl first ds) 
  report first first sort-by [(last ?1) > (last ?2)] atr1
end 

; Devuelve el atributo de máxima ganancia de un dataset
to-report Max-GR [ds]
  let atr1 map [(list ? (ifelse-value ((entropia bf columna ? ds) = 0) [0] [(GI ds ?) / (entropia bf columna ? ds)]))] (bl first ds) 
  report first first sort-by [(last ?1) > (last ?2)] atr1
end 

; Devuelve si un Dataset está ya calsificado
to-report clasificado? [ds]
  let clas (last first ds)
  let val bf (columna clas ds)
  let vals length remove-duplicates val
  report ifelse-value (vals = 1) [true] [false]
end

to layout
  layout-spring vertices aristas .5 8 .3
  let cx sum [xcor] of vertices / count vertices
  let cy sum [ycor] of vertices / count vertices
  ask vertices [
    setxy (xcor - cx) (ycor - cy)
    setxy xcor * .999 ycor * .999]
end

;Devuelve la proporción para HD o no
to-report getCor [cor]
  report cor * 4 / patch-size
end
@#$#@#$#@
GRAPHICS-WINDOW
190
10
1002
443
-1
-1
2.0
1
10
1
1
1
0
0
0
1
0
400
0
200
0
0
1
ticks
30.0

BUTTON
0
80
55
113
NIL
pista
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
350
445
460
478
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
120
210
153
345
pedal
pedal
0
100
77
1
1
NIL
VERTICAL

SLIDER
0
210
120
243
girar
girar
-360
360
268
1
1
NIL
HORIZONTAL

BUTTON
5
280
60
313
<
set girar girar - 5\nset final \"girar-i\"
NIL
1
T
OBSERVER
NIL
K
NIL
NIL
1

BUTTON
60
280
115
313
>
set girar girar + 5\nset final \"girar-d\"
NIL
1
T
OBSERVER
NIL
Ñ
NIL
NIL
1

BUTTON
35
245
90
278
^
if pedal < 300 [set pedal pedal + 1]\n
NIL
1
T
OBSERVER
NIL
O
NIL
NIL
1

BUTTON
35
315
90
348
-
if pedal > 0 [set pedal pedal - 1]\n
NIL
1
T
OBSERVER
NIL
L
NIL
NIL
1

SLIDER
155
210
188
345
marcha
marcha
-1
3
1
1
1
NIL
VERTICAL

BUTTON
55
80
110
113
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

CHOOSER
5
420
115
465
area-seleccionada
area-seleccionada
"Meta"
0

BUTTON
115
420
190
465
Marcar area
marcar-area area-seleccionada
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
5
465
190
498
Restablecer
restablecer-areas
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
110
80
190
113
TODO
ca\npista\nsetup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
1005
225
1150
258
tasa-aprendizaje
tasa-aprendizaje
0.0
1.0
0.2
1.0E-4
1
NIL
HORIZONTAL

BUTTON
1005
260
1080
293
NIL
entrenar
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
1150
175
1310
295
Error vs. Epochs
Epochs
Error
0.0
10.0
0.0
0.5
true
false
"" ""
PENS
"default" 1.0 0 -7500403 true "" ""

BUTTON
1120
105
1250
138
NIL
almacena
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
1010
175
1090
193
Red Neuronal  ¬
11
0.0
1

BUTTON
1005
140
1105
173
cargar
cargar user-file
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1105
140
1210
173
guardar
guardar user-new-file
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
255
445
347
478
cpu
cpu
100
2000
700
100
1
NIL
HORIZONTAL

CHOOSER
1120
60
1310
105
salida
salida
"curva-i" "curva-d" "cortado" "leve" "recta"
3

BUTTON
1210
140
1310
173
vaciar
set data-list []
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
60
175
190
208
piloto-auto?
piloto-auto?
0
1
-1000

BUTTON
460
445
535
478
inicio
ask coches[ \n    move-to patch getCor 105 getCor 10\n    set heading -90]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1120
10
1310
43
Tomar muestra del terreno
terreno\nprint color-pista
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
5
350
60
383
BEEP!
;sound:play-sound \"sounds/bocina.wav\"\nsound:play-note \"TRUMPET\" 60 64 0.3\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
1025
25
1095
105
IT
70
65.0
1

CHOOSER
0
10
95
55
mapa
mapa
"wipeout" "desert" "dukes" "roads" "circuit"
0

SWITCH
95
10
190
43
HD?
HD?
0
1
-1000

SWITCH
95
45
190
78
intro?
intro?
1
1
-1000

BUTTON
535
445
610
478
random
ask coches[ \n    move-to one-of patches\n    set heading -90]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
1010
305
1160
323
ID3
11
0.0
1

BUTTON
750
445
865
478
checkpoints
setup-checkpoints
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
1005
190
1150
223
nodos-capa-oculta
nodos-capa-oculta
1
20
15
1
1
NIL
HORIZONTAL

BUTTON
130
350
190
383
M+
if marcha < 3 [set marcha marcha + 1]\nif marcha = 0 [ set final \"0\"]\nif marcha = 1 [ set final \"1ª\"]\nif marcha = 2 [ set final \"2ª\"]\nif marcha = 3 [ set final \"3ª\"]\nif marcha = -1 [ set final \"R\"]
NIL
1
T
OBSERVER
NIL
P
NIL
NIL
1

BUTTON
70
350
130
383
M-
if marcha > -1 [set marcha marcha - 1]\nif marcha = 0 [ set final \"0\"]\nif marcha = 1 [ set final \"1ª\"]\nif marcha = 2 [ set final \"2ª\"]\nif marcha = 3 [ set final \"3ª\"]\nif marcha = -1 [ set final \"R\"]
NIL
1
T
OBSERVER
NIL
I
NIL
NIL
1

BUTTON
1250
105
1310
138
deshacer
set data-list remove-item (length data-list - 1) data-list
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
865
445
920
478
<--
if count checkpoints = 0 and count coches with [[area] of patch-here = \"Meta\"] > 0 [setup-checkpoints]\n
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
610
445
685
478
grua
ask coches [move-to patch-ahead -10]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1090
260
1150
293
NIL
olvidar
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
1140
45
1290
63
Añadir datos  ¬
11
0.0
1

TEXTBOX
5
195
55
213
Manual  ¬
11
0.0
1

CHOOSER
1170
320
1275
365
Max.Metodo
Max.Metodo
"Ganancia Información" "Razón de Ganancia"
1

OUTPUT
1005
390
1330
505
6

BUTTON
1005
320
1060
353
ID3
ID3-main
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1275
320
1330
365
layout
;layout\n\nask vertices [move-to one-of patches with [area = \"ID3\" and count turtles in-radius getCor 4 = 0]]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
1060
320
1170
353
id3-on?
id3-on?
1
1
-1000

SWITCH
0
140
190
173
rec?
rec?
1
1
-1000

BUTTON
1005
355
1060
388
load
ask vertices [die]\nclear-output\nlet aux load-dataset\noutput-print output-dataset dataset
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1060
355
1115
388
save
save user-new-file
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1115
355
1170
388
clear
set dataset []\nclear-output
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## WHAT IS IT?

Mi propuesta para la matricula de honor.

IT es un vehiculo con inteligencia personalizable. 
 - Tomando una muestra del terreno, puede adaptarse a cualquier tipo del terreno
 - Puede distinguir la vía si le enseñas ejemplos de curvas, rectas, bloqueos
 - Manejandolo, puedes enseñarle a comportarse ante esos elementos y a moverse y no salirse de la vía

## HOW IT WORKS

El vehículo dispone de un scanner en su parte delantera que registra el color de los patches que tiene en un cuadrado de 7x7 delante suya. Estos datos son procesados de manera que los que estan en la gama del terreno muestreado serán de color blanco y el resto negro.

Esta cuadrícula de patches es enviada a la red neuronal que tiene la tarea de aproximar esas figuras a curvas, rectas, pasos bloqueados, caminos peligrosos... Estos datos juntos con el radar que se encarga de decir la orientación del próximo punto del camino formarán el conjunto de estados donde tendrá que moverse el algoritmo de decisión de las acciones a ejecutar.

IT decide la mejor acción mediante el algoritmo ID3 y esta es ejecutada. También existe la posibilidad de tomar las acciones mediante unas condiciones básicas que refuerzan las aristas dando lugar a activaciones en las acciones finales. Esta forma la he utilizado sobre todo para testeo y mejora del ID3.


## HOW TO USE IT

- TODO
- Muestrea terreno
- Entrena red neuronal (carga fichero o recorre la pista tu mismo almacenando)
- Graba tu comportamiento o carga uno y ejecuta ID3. También puedes utilizar las decisiones básicas.
- Go
- Opcional: Usar checkpoints para activar el radar, cambia de mapa, resolución (HD?), etc. Todo esto afecta al comportamiento y a lo que se puede entrenar

## THINGS TO NOTICE

La toma de decisiones básica funciona mejor si el dataset de ID3 no detalla bien las acciones

## THINGS TO TRY

Prueba a activar o desactivar el piloto automático (IA)
Con IA activa puedes usar las dos versiones de toma de decisiones diferentes
Usar o no usar checkpoints para ver la eficacia de la red
Probar otras pistas con otros terrenos (desplegable mapa)
Usa la bocina

SI ME QUEDO ATASCADO, USA LA GRÚA UNA O DOS VECES... O AYUDAME!

## EXTENDING THE MODEL

El sistema de decisión básica se pensó para ejecutar Q-learning. 

## RELATED MODELS

el Hombre Orquesta
Spuffel

## CREDITS AND REFERENCES

by Rafa TM
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

0
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -16777216 true false 60 135 240 180

1ra
false
0
Rectangle -7500403 true true 0 0 300 300
Polygon -11221820 true false 135 255 135 180 120 180 150 120 180 180 165 180 165 255 135 255

2da
false
0
Rectangle -7500403 true true 0 0 300 300
Polygon -13791810 true false 120 255 120 165 90 165 150 90 210 165 180 165 180 255 120 255

3ra
false
0
Rectangle -7500403 true true 0 0 300 300
Polygon -13345367 true false 105 255 105 120 60 120 150 45 240 120 195 120 195 255 105 255

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 105 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

car top
true
0
Polygon -7500403 true true 151 8 119 10 98 25 86 48 82 225 90 270 105 285 150 285 195 285 210 270 219 225 214 47 201 24 181 11
Polygon -16777216 true false 210 195 195 195 195 150 210 105
Polygon -16777216 true false 105 270 135 255 165 255 195 270 195 240 105 240
Polygon -16777216 true false 90 195 105 195 105 165 90 105
Polygon -1 true false 205 29 180 30 181 11
Line -7500403 true 210 165 195 165
Line -7500403 true 90 165 105 165
Polygon -16777216 true false 120 180 180 180 204 97 182 89 153 85 120 89 98 97
Line -16777216 false 210 90 195 30
Line -16777216 false 90 90 105 30
Polygon -1 true false 95 29 120 30 119 11
Line -16777216 false 120 195 180 195
Line -16777216 false 120 210 180 210
Line -16777216 false 120 225 180 225

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cortado
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -2674135 true false 0 0 300 210

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

curva-d
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -2674135 true false 0 0 300 90
Rectangle -2674135 true false 0 0 90 300
Rectangle -2674135 true false 210 210 300 300

curva-i
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -2674135 true false 0 0 300 90
Rectangle -2674135 true false 210 0 300 300
Rectangle -2674135 true false 0 210 90 300

cylinder
false
0
Circle -7500403 true true 0 0 300

decision
false
0
Polygon -7500403 true true 0 150 150 0 300 150 150 300 0 150

desvio-d
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -2674135 true false 210 0 315 90
Rectangle -2674135 true false 0 0 90 300
Rectangle -2674135 true false 210 210 300 300

desvio-i
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -2674135 true false -15 0 90 90
Rectangle -2674135 true false 210 0 300 300
Rectangle -2674135 true false 0 210 90 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

girar-d
false
0
Rectangle -7500403 true true 0 0 300 300
Polygon -13840069 true false 225 120 180 75 180 105 150 105 120 120 105 135 90 165 90 195 120 195 120 165 135 150 150 135 180 135 180 165 225 120

girar-i
false
0
Rectangle -7500403 true true 0 0 300 300
Polygon -955883 true false 75 120 120 75 120 105 150 105 180 120 195 135 210 165 210 195 180 195 180 165 165 150 150 135 120 135 120 165 75 120

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

it
true
0
Polygon -7500403 true true 120 105 135 90 165 90 180 105 180 195 165 210 135 210 120 195 120 105
Rectangle -16777216 true false 105 165 120 195
Rectangle -16777216 true false 105 105 120 120
Rectangle -16777216 true false 180 105 195 120
Rectangle -16777216 true false 180 165 195 195
Circle -5825686 true false 129 144 42
Line -13345367 false 135 135 165 135
Line -13345367 false 135 120 165 120
Line -13345367 false 135 180 135 210
Line -13345367 false 165 180 165 210

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

leve
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -2674135 true false 75 60 105 105
Rectangle -2674135 true false 45 180 255 195
Rectangle -2674135 true false 90 240 120 255
Rectangle -2674135 true false 180 210 210 240
Rectangle -2674135 true false 120 30 285 45
Line -2674135 false 105 135 255 135
Rectangle -2674135 true false 0 0 60 300
Rectangle -2674135 true false 240 0 300 300

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

r
false
0
Rectangle -7500403 true true 0 0 300 300
Polygon -8630108 true false 135 45 135 180 120 180 150 255 180 180 165 180 165 45 135 45

radar
true
0
Polygon -7500403 true true 150 150 240 45 255 60 270 75 285 120 285 150 285 180 270 225 255 240 240 255 150 150

recta
false
0
Rectangle -7500403 true true 0 0 300 300

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
1
@#$#@#$#@
