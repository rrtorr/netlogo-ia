extensions [matrix]

;Los estados serán agentes
breed[estados estado]

estados-own[
  contenido                          ; contiene la matriz que representa al mundo
  camino                             ; contiene el camino hasta él, creo que no hará falta
  activo                             ; si está activo o no
  evaluado                           ; si se ha evaluado o no en la vuelta MiniMax
  padre                              ; quien es el padre, osea del que procede. Con esto, creo que camino no hace falta
  final                              ; si es un estado final del árbol
  nivelArbol                         ; nivel al que pertenece el estado dentro del arbol de estados
  valor                              ; valor max-min del estado
  turno                              ; si es jugada de MAX(máquina 1) o de Min(jugador -1)

]
breed [
  huecos
]

breed [
  fichas
]

globals [
  profundidadDeNivel
  estadoEnJuego
  finDeJuego
  hecho
  partida
]

to setup
  clear-all
  clear-ticks
  resize-world (resizeX / 2 * -1) (resizeX / 2) (resizeY / 2 * -1) (resizeY / 2)
  generaEstadoInicial
  set estadoEnJuego estado 0
  tablero
  set finDeJuego 2
  set hecho false
  set profundidadDeNivel 3
end

to tablero
  ask patches [
    sprout-huecos 1 [ set color white set shape "circle" set size 0.8]
    if pxcor mod 2 = 0 [set pcolor 45]
    if pxcor mod 2 = 1 [set pcolor 44]
   ]
end

to jugar                                         ; Bucle de turnos jugador-maquina
  if-else mouse-down? [
    meteFicha                                    ; JUGADOR: Crea una ficha según el click en coordenada del tablero
    juez                                         ; Función independiente de MiniMax que observa el tablero y para la partida si alguien ha ganado, coloca banderitas y pone Victoria
    if partida != 0 [stop]                       ; Se para segun dijo el juez
    ;if esFinal generaMatriz != 2 [stop]         ; Similar pero sin juez, con la funcion es fina,l para que se entienda bien q es lo mismo y ademas probarla de nuevo
    adversario                                   ; ADVERSARIO: Esta es la función que primero toma el estado actual del tablero, se lo manda a minimax, y llama a generaMovimiento con la coordenada obtenida
    juez                                         ; Se vuelve a ejecutar juez
    ;if esFinal generaMatriz != 2 [stop]
    if partida != 0 [stop]                       ; Se para segun juez
  ]
  [set hecho false]                              ;Con este parametro evito q se ejecute más de una vez por los milisegundos que se tarda en soltar el click, lo modifica la funcion meteFicha
end

to meteFicha
  if hecho = false [
    create-fichas 1 [
      set shape "circle"
      set size 0.8
      set heading 180
      set color black
      move-to patch mouse-xcor max-pycor
      wait 0.1
      repeat (max-pycor - min-pycor) [caer]
    ]
    set hecho true
  ]
end

to adversario
  ask estadoEnJuego [
    set contenido generaMatriz
    set activo true                              ;**************************** modificacion
    set turno -1
    set final false
    set evaluado false
    set valor 0
    ;set padre self
    set estadoEnJuego miniMax estadoEnJuego
  ]
  ;print generaMatriz
  ;print [contenido] of estadoEnJuego
  let moverseA generaMovimiento [contenido] of estadoEnJuego
  ;print moverseA
  create-fichas 1 [
      set shape "circle"
      set size 0.8
      set heading 180
      set color red
      move-to patch moverseA max-pycor
      ;move-to patch random-pxcor max-pycor
      wait 0.1
      repeat (max-pycor - min-pycor) [caer]
    ]
end

to caer
  if ycor != min-pycor and [count fichas-here] of patch-ahead 1 = 0[
    move-to patch-ahead 1
    wait 0.10
  ]
end


;************************************************ RAFA ***********************************************************************

;; genera estado inicial
to generaEstadoInicial
   create-estados 1                                ;modifico sus valores
 [
   set contenido generaMatriz
   set camino (list self)                        ; en camino meto el camino hasta su padre
   set activo true                               ; lo pongo como activo
   set evaluado false                            ; lo pongo a falso
   set padre self                             ; fijo quien es su padre
   set final false
   set nivelArbol 0
   set valor 0                                   ; pongo su valor a cero
   set turno -1
   set hidden? true]                             ; pongo en turno el jugador
end

;; Genera Matriz a partir del tablero
to-report generaMatriz
  let m matrix:make-constant (max-pycor - min-pycor + 1) (max-pxcor - min-pxcor + 1) 0
  let i 0
  let j 0
  let fila []
  let valorm 0
  repeat (max-pycor - min-pycor + 1)[
    set fila []
    set i 0
    repeat (max-pxcor - min-pxcor + 1)[
      if [count fichas-here] of patch (min-pxcor + i) (max-pycor - j) = 0 [ set valorm 0]
      if [count fichas-here with [color = red]] of patch (min-pxcor + i) (max-pycor - j) > 0 [ set valorm -1]
      if [count fichas-here with [color = black]] of patch (min-pxcor + i) (max-pycor - j) > 0 [ set valorm 1]
      set fila lput valorm fila
      set i i + 1
    ]
    matrix:set-row m j fila
    set j j + 1
  ]
  report m
end

;; Genera un movimiento de ficha a partir del cambio entre dos estados
to-report generaMovimiento [matriz]
  let inicial generaMatriz
  print (word "inicial: " inicial " <=== ASI ESTABA EL TABLERO")
  print (word "estado: " matriz " <=== ESTO FUE LO Q ME DIO MINIMAX")
  let resultado matrix:minus inicial matriz
  print (word "resultado: " resultado " <=== DEBERÍA HABER SOLO UNO DISTINTO DE CERO")
  let coordenada 999
  let i 0
  let j 0
  let fila []
  let valorm 0
  repeat (max-pycor - min-pycor + 1)[
    set i 0
    repeat (max-pxcor - min-pxcor + 1)[
      ;print  matrix:get resultado j i
      if matrix:get resultado j i != 0 [set coordenada (min-pxcor + i)]
      set i i + 1
    ]
    set j j + 1
  ]
  print (word "coordenada obtenida: " coordenada)
  report coordenada
end

to juez
  ask patches with [count fichas-here > 0] [
    let tempColor [color] of one-of fichas-here
    let validoH true let validoV true let valido1 true let valido2 true
    let listaH [] let listaV [] let lista1 [] let lista2 []
    let i 0
    repeat numero - 1[
      set i i + 1
      ;; Horizontal
      if-else (pxcor + i) > max-pxcor [set validoH false] [if ([count fichas-here with [color = tempColor]] of patch (pxcor + i) pycor) < 1 [set validoH false]]
      set listaH lput (patch (pxcor + i) pycor) listaH
      ;; Vertical
      if-else (pycor + i) > max-pycor [set validoV false] [if ([count fichas-here with [color = tempColor]] of patch pxcor (pycor + i)) < 1 [set validoV false]]
      set listaV lput (patch pxcor (pycor + i)) listaV
      ;; Diagonal 1
      if-else (pxcor + i) > max-pxcor or (pycor + i) > max-pycor [set valido1 false] [if ([count fichas-here with [color = tempColor]] of patch (pxcor + i) (pycor + i)) < 1 [set valido1 false]]
      set lista1 lput (patch (pxcor + i) (pycor + i)) lista1
      ;; Diagonal 2
      if-else (pxcor + i) > max-pxcor or (pycor - i) < min-pycor [set valido2 false] [if ([count fichas-here with [color = tempColor]] of patch (pxcor + i) (pycor - i)) < 1 [set valido2 false]]
      set lista2 lput (patch (pxcor + i) (pycor - i)) lista2
    ]
    if validoH = true [ foreach listaH [ask [fichas-here] of ? [set shape "flag"] ask fichas-here [set shape "flag"]]]
    if validoV = true [ foreach listaV [ask [fichas-here] of ? [set shape "flag"] ask fichas-here [set shape "flag"]]]
    if valido1 = true [ foreach lista1 [ask [fichas-here] of ? [set shape "flag"] ask fichas-here [set shape "flag"]]]
    if valido2 = true [ foreach lista2 [ask [fichas-here] of ? [set shape "flag"] ask fichas-here [set shape "flag"]]]

    if validoH = true or validoV = true or valido1 = true or valido2 = true [set partida "Victoria" stop]
  ]
end

to-report esFinal [matrizEstadoActual]
  let res 0
  let dim matrix:dimensions matrizEstadoActual
  let i 0
  let j 0
  let k 0
  let valorm 0
  let validoH true let validoV true let valido1 true let valido2 true
  repeat item 1 dim[
    set i 0
    repeat item 0 dim[
      set valorm matrix:get matrizEstadoActual j i
      set validoH true set validoV true set valido1 true set valido2 true
      if (matrix:get matrizEstadoActual j i) != 0 [
        set k 0
        repeat numero - 1[
          set k k + 1
          ;; Horizontal
          if-else (i + k) > (item 0 dim - 1) [set validoH false] [if (matrix:get matrizEstadoActual j (i + k) ) != valorm [set validoH false]]
          ;; Vertical
          if-else (j + k) > (item 1 dim - 1) [set validoV false] [if (matrix:get matrizEstadoActual (j + k) i ) != valorm [set validoV false]]
          ;; Diagonal 1
          if-else (i + k) > (item 0 dim - 1) or (j + k) > (item 1 dim - 1) [set valido1 false] [if (matrix:get matrizEstadoActual (j + k) (i + k) ) != valorm [set valido1 false]]
          ;; Diagonal 2
          if-else (i + k) > (item 0 dim - 1) or (j - k) < 0 [set valido2 false] [if (matrix:get matrizEstadoActual (j - k) (i + k) ) != valorm [set valido2 false]]
        ]
        if validoH = true or validoV = true or valido1 = true or valido2 = true [report (valorm * -1) stop]
      ]
      set i i + 1
    ]
    set j j + 1
  ]
  set i 0
  set j 0
  let relleno true
  set valorm matrix:get matrizEstadoActual j i
  repeat item 1 dim [
    set i 0
    repeat item 0 dim [
      if (matrix:get matrizEstadoActual j i) = 0 [set relleno false]
      set i i + 1
    ]
    set j j + 1
  ]
  if-else relleno = true [report 0] [report 2]
end

;************************************ FIN RAFA ******************************************

;****************************** MIniMax *************************************
to-report miniMax [estadoActual]                    ;tiene que recibir un estado con la jugada de Min (jugador)
                                                    ;y tiene que devolver un estado con la jugada de Max
                                                    ;el estado que recibe habra que establecerle los valores iniciales

ask estadoActual [set nivelArbol 0]
print(word "********** Estado que recibe MiniMax******************")
print(word matrix:pretty-print-text [contenido] of estadoActual)
print(word "Nivel del Arbol " [nivelArbol] of estadoActual)
print(word "Valor           " [valor] of estadoActual)
print(word "Turno           " [turno] of estadoActual)
print(word "Activo           "[activo] of estadoActual)
print(word "******************************************************")

let nivelTope nivelArbol + profundidadDeNivel       ; fijo el máximo número de niveles a explorar
let profundidadArbolMax nivelTope
let contador 0

show (word "***************Nivel Arbol recibido " nivelArbol)

While [any? estados with [activo] and nivelArbol <= nivelTope]
[

  ask estados with [activo  and nivelArbol <= nivelTope] [

    set activo false                                               ; lo pongo como no activo


;    print(word "Voy a crear Alternativas de la MAtriz " matrix:pretty-print-text contenido)
;    print(word " con la ficha " ficha " y estando en el nivel " nivelArbol)
    let listaMatrices generaMatrices contenido turno              ;en listaMatrices meto las matrices que me ha devuelto
                                                                   ;generaMatrices pasandole el contenido del estado actual y el valor contrario del turnoactual
                                                                   ;contenido contiene la matriz que lo forma

    foreach listaMatrices                             ;para cada una de las matrices que he recibido
    [                                                 ;le establezco los valores de sus variables
      hatch-estados 1
      [
        set contenido ?                               ; en contenido meto la matriz
        set camino lput self camino                   ; en camino meto el camino hasta su padre
        set activo true                               ; lo pongo como activo
        set evaluado false                            ; lo pongo a falso
        set padre myself                              ; fijo quien es su padre
;        show (word "El padre es " padre)
        set nivelArbol nivelArbol + 1
        set valor 0
        set turno turno * -1                          ; pongo en turno el contrario al que tenía
                                                      ; el valor lo tengo que calcular cuando sea estado final o haya llegado a un nivel de exploración máximo


        let valorFinal esFinal ?                              ; calculo si es final, devuelve 1->ganaMax  -1->ganaMin
                                                     ;                               0->empate    2->no es final
        ifelse (valorFinal != 2)                      ; si es distinto de 2 es que hemos llegado a un estado final
        [                                             ; tenemos que hacer que no siga explorando y darle su valor
          set final true                              ; que será el propio del estado final, quien ha ganado o empate
          set activo false
          set valor valorFinal]


        [
          if(nivelArbol = nivelTope)                  ; si es 2, tenemos que comprobar si hemos explorado hasta el nivel
          [                                           ; de profundidad establecido. Si es así consideramos los estados como finales
            set final true                            ; hacemos que no siga explorando, y le fijamos en valor con la función objetivo
            set activo false                          ; que consistirá en la diferencia de las posibilidades de cada uno
            set valor objetivo ?
            set contador contador + 1
            ;print(word "Es una Matriz final " matrix:pretty-print-text ? " Con objetivo " valor "  Numeros Finales " contador)



            ]]
 ;        print(word "Matriz final " matrix:pretty-print-text ? " Con objetivo " valor )
      ]

         print(word "********** Estado generados ***************************")
         print(word matrix:pretty-print-text contenido)
         print(word "Nivel del Arbol " nivelArbol)
         print(word "Valor           " valor)
         print(word "Turno           " turno)
         print(word "Activo           "activo)
         print(word "*******************************************************")


    ];FIN foreach
  ];fin ask
];FIN While


; ////////////////////// AQUI EMPIEZA EL MINIMAX //////////////////////////////
;show(word"ProfundidadArbolMax " profundidadArbolMax)
While [profundidadArbolMax > 1]                                       ; recorro todo el arbol hasta la profundidad 2,
                                                                      ; una vez termine devolveré el que tenga máximo valor de la profundidad 1
[
;  show(word "He entrado en el While")

;  show (word "Entoy en el While y los estados finales son " estados with [final = true and nivelArbol = profundidadArbolMax]);nivelArbol = nivelTope]


  ask estados with [final = true and nivelArbol = profundidadArbolMax];nivelTope]  ;meto en una lista todos los estados finales
  [                                                                   ;de la misma profundidad

    let valorHijo valor                                               ;guardo el valor del hijo
    set final false                                                   ;pongo al hijo como no final
    set evaluado true                                                 ;digo que ya lo he evaluado

    ifelse(turno = 1)                                                 ;si es uno es Max, si no es uno es Mix
    [
      ask padre [                                                     ;es un Max
        set valor max(list valor valorHijo)                           ;modifico el valor del padre con el máximo de los dos
        set final true]]                                              ;establezco al padre ahora como final

    [
      ask padre [                                                     ;es un Min
        set valor min(list valor valorHijo)                           ;modifico el valor del padre con el mínimo de los dos
        set final true]]                                              ;establezco el padre ahora como final
  ]
  set profundidadArbolMax profundidadArbolMax - 1                     ;decremento la profunddidad del árbol
                                                                     ;final while// cuando termina el while, se supone que estoy en el primer nivel
                                                                      ;y que tengo a todos esos nodos como hijos, voy a devolver el estado que tenga
]


ask estados with [final = false and nivelArbol != 0][
  die]



 let estadoADevolver max-one-of estados [valor]

print(word "********** Estado que devuelve MiniMax******************")
print(word matrix:pretty-print-text [contenido] of estadoADevolver)
print(word "Nivel del Arbol " [nivelArbol] of estadoADevolver)
print(word "Valor           " [valor] of estadoADevolver)
print(word "Turno           " [turno] of estadoADevolver)
print(word "******************************************************")





report estadoADevolver
end







;******************************* fin MiniMax ***********************************************






;******************************* fin MiniMax ***********************************************



;************************************ OSCAR **********************************************

to-report objetivo [matriz-estado-actual]
  let m matrix:copy matriz-estado-actual

  let X last matrix:dimensions m - 1     ; nº de columnas (0..n)
  let Xm 0
  let Y 0
  let Ym first matrix:dimensions m - 1   ; nº de filas (0..n)

  let total_jugadasMax 0                 ; serán los elementos que están a 1, los variables tienen un 1
  let total_jugadasMin 0                 ; serán los elementos que están a -1, las variables tienen un 2

  ; H: Horizontal, V: Vertical, D: Diagonal, I: Diagonal inversa
  let tot1H 0 let tot1V 0 let tot1dD 0 let tot1dI 0
  let tot2H 0 let tot2V 0 let tot2dD 0 let tot2dI 0

  let filas first matrix:dimensions m - 1    ; filas (0..n)
  let columnas last matrix:dimensions m - 1  ; columnas (0..n)
  let f 0
  while [f <= filas] [
    let c 0
    while [c <= columnas] [
      ; print (word "Coordenada: (" f ", " c ")")

      let valorElem matrix:get m f c

      ifelse (valorElem = 1) [

        let cont1dH 0  let cont1iH 0  let cont1V 0  let cont1dD 0  let cont1dI 0  ; contiena una línea H/V/D/I
        let sigue1dH true  let sigue1iH true  let sigue1V true  let sigue1dD true  let sigue1dI true   ; continúa una línea H/V/D/I
        foreach [1 2]
        [
          if((c + ?) <= X) [
            set valorElem matrix:get m f (c + ?)
            ifelse (valorElem >= 0 and sigue1dH) [set cont1dH cont1dH + 1][set sigue1dH false]]
          if((c - ?) >= xm) [
            set valorElem matrix:get m f (c - ?)
            ifelse (valorElem >= 0 and sigue1iH) [set cont1iH cont1iH + 1][set sigue1iH false]]
          if((f - ?) >= Y) [
            set valorElem matrix:get m (f - ?) c
            ifelse (valorElem >= 0 and sigue1V) [set cont1V cont1V + 1][set sigue1V false]]
          if((c + ?) <= X and (f - ?) >= Y) [
            set valorElem matrix:get m (f - ?) (c + ?)
            ifelse (valorElem >= 0 and sigue1dD) [set cont1dD cont1dD + 1][set sigue1dD false]]
          if((c - ?) >= xm and (f - ?) >= Y) [
            set valorElem matrix:get m (f - ?) (c - ?)
            ifelse (valorElem >= 0 and sigue1dI) [set cont1dI cont1dI + 1][set sigue1dI false]]
        ]
        ifelse (cont1dH + cont1iH > 2)[set tot1H (cont1dH + cont1iH - 2)][set tot1H 0]
        ifelse (cont1V = 3) [set tot1V 1][set tot1V 0]
        ifelse (cont1dD = 3) [set tot1dD 1][set tot1dD 0]
        ifelse (cont1dI = 3) [set tot1dI 1] [set tot1dI 0]

        set total_jugadasMax total_jugadasMax + tot1H + tot1V + tot1dD + tot1dI

      ][ ; else

         if (valorElem = -1) [

           let cont2dH 0  let cont2iH 0  let cont2V 0  let cont2dD 0  let cont2dI 0
           let sigue2dH true  let sigue2iH true  let sigue2V true  let sigue2dD true  let sigue2dI true
           foreach [1 2 3]
           [
             if((c + ?) <= X) [
               set valorElem matrix:get m f (c + ?)
               ifelse (valorElem <= 0 and sigue2dH) [set cont2dH cont2dH + 1][set sigue2dH false]]
             if((c - ?) >= xm) [
               set valorElem matrix:get m f (c - ?)
               ifelse (valorElem <= 0 and sigue2iH) [set cont2iH cont2iH + 1][set sigue2iH false]]
             if((f - ?) >= Y) [
               set valorElem matrix:get m (f - ?) c
               ifelse (valorElem <= 0 and sigue2V) [set cont2V cont2V + 1][set sigue2V false]]
             if((c + ?) <= X and (f - ?) >= Y) [
               set valorElem matrix:get m (f - ?) (c + ?)
               ifelse (valorElem <= 0 and sigue2dD) [set cont2dD cont2dD + 1][set sigue2dH false]]
             if((c - ?) >= xm and (f - ?) >= Y) [
               set valorElem matrix:get m (f - ?) (c - ?)
               ifelse (valorElem <= 0 and sigue2dI) [set cont2dI cont2dI + 1][set sigue2dH false]]
           ]
           ifelse (cont2dH + cont2iH > 2)[set tot2H (cont2dH + cont2iH - 2)][set tot2H 0]
           ifelse (cont2V = 3) [set tot2V 1][set tot2V 0]
           ifelse (cont2dD = 3) [set tot2dD 1][set tot2dD 0]
           ifelse (cont2dI = 3) [set tot2dI 1][set tot2dI 0]

           set total_jugadasMin total_jugadasMin + tot2H + tot2V + tot2dD + tot2dI

         ]
      ]
      set c (c + 1)
    ]
    set f (f + 1)
  ]

   ; show (word "Total jugadas MAX: " total_jugadasMax "    Total jugadas MIN: " total_jugadasMin)
   report total_jugadasMax - total_jugadasMin
end

;************************************** FIN OSCAR ***************************************************

;************************************ OSCAR **********************************************


to-report generaMatrices [matriz-estado-actual ficha]
  let m matrix:copy matriz-estado-actual

  ;; Genera n copias idénticas (una por alternativa)
  ; n = columnas que tiene el tablero con huecos utilizables
  let num_alternativas_max last matrix:dimensions m  ; num_alternativas_max: total de columnas que tiene el tablero
  let n num_alternativas_max
  let i 0  ; índice que indica la columna que se está procesando
  let copias [ ]  ;lista que contendrá una copia idéntica de la matriz original por cada posible alternativa
  let alternativas [ ]  ; lista con las alternativas disponibles (la que devuelverá la función)

  repeat num_alternativas_max [
    let valor_hueco_mas_alto matrix:get m 0 i
    ifelse (valor_hueco_mas_alto = 0) [
      ; Añade copia de la matriz de estado actual al final de la lista de copias
      let copia matrix:copy m
      set copias lput copia copias  ; si hay hueco arriba del todo es que almenos habrá un hueco libre
    ]
    [ ;else
      set n (n - 1)
    ]
    set i (i + 1)
  ]

  ;; Ahora procesaremos la lista de copias, para ir modificando cada copia y convirtiéndola en una de las alternativas
  ifelse (empty? copias = false) [
    set i 0
    repeat n [
      ; Recuperamos de la lista de copias la correspondiente a la alternativa en proceso
      let alternativa_n item i copias

      ; Buscamos hueco libre en la columna
      let columna matrix:get-column alternativa_n i  ; columna concreta que se modificará para esta alternativa
      let j (length columna - 1)  ; nº de filas totales que tiene el tablero (0..n)
      ; Comprobamos cada item de la lista (la columna de la alternativa) de abajo a arriba hasta dar con el primero que vale 0 (casilla vacía)
      while [item j columna != 0] [
        set j (j - 1)
      ]

; Coloca un "-1" (ficha de IA) en la posición más baja utilizable (con 0) de la columna
      set alternativa_n (matrix:set-and-report alternativa_n j i ficha)
 ;     print (word "Alternativa nº" i ":\n" matrix:pretty-print-text alternativa_n)

      ; Añade la alternativa a la lista de alternativas
      set alternativas lput alternativa_n alternativas

      set i (i + 1)
    ]
  ]
  [ ; else
    ;;print "¡¡¡NO HAY ALTERNATIVAS!!!."
  ]
  report alternativas
end


;************************************** FIN OSCAR ***************************************************
@#$#@#$#@
GRAPHICS-WINDOW
189
10
619
461
3
3
60.0
1
10
1
1
1
0
0
0
1
-3
3
-3
3
0
0
1
ticks
30.0

BUTTON
16
98
79
131
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
89
98
152
131
NIL
jugar
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
16
187
152
288
PARTIDA
partida
17
1
25

SLIDER
16
142
152
175
numero
numero
1
7
4
1
1
NIL
HORIZONTAL

TEXTBOX
20
305
170
375
Dale a la columna donde quieras echar ficha!
11
0.0
1

SLIDER
16
11
152
44
resizeX
resizeX
1
29
7
2
1
NIL
HORIZONTAL

SLIDER
16
52
152
85
resizeY
resizeY
1
29
7
2
1
NIL
HORIZONTAL

@#$#@#$#@
## Juego Conecta-4 basado en algoritmo MiniMax

Modelo del clásico juego Conecta4 donde debes "conectar" 4 fichas del mismo color, ya sea en vertical, horizontal o diagonal, antes de que lo haga tu oponente.

En este modelo la IA corre a cargo de un algoritmo MiniMax con restricción de la profundidad del árbol de decisión fijado a 2 niveles.

## COMO TRABAJA

Consiste en jugar una persona contra la máquina. Empezará jugando la persona, seleccionando para ello un botón de cualquiera de las columnas.
Tenemos la opción de elegir el nivel de dificultad. Este nivel hará que la máquina explore el árbol con profundidad variable. Si lo pusieramos a tope exploraría todas las opciones. No hay que decir que a mayor exploración más tardará en responder la máquina.

## CARACTERÍSTICAS IMPORTANTES

La interface gráfica permite jugar utilizando únicamente el cursor del ratón, pulsando sobre cualquier lugar dentro de la columna en donde se quiere realizar el movimiento de ficha.

Cuando un jugador conecta cuatro fichas en linea, éstas cambiarán por 4 banderas indicando la jugada que da la victoria al jugador ganador.

## A TENER EN CUENTA

* El número de filas y columnas del tablero ha de ser impar
* De momento únicamente se puede jugar al Conecta4 original (conectando cuatro fichas en linea)

## THINGS TO TRY

* ¡Intenta ganar a la IA!
* ¡Intenta ganar a la IA en el menor número de movimientos posibles!

## EXTENDING THE MODEL

* Añadir opción de 2 jugadores humanos
* Añadir opción de 2 jugadores IA
* Poder configurar por completo el taaño de tablero
* Poder configurar el número de fichas en línea que hace falta conectar (4 por defecto).
* Poder configurar el nivel de profundidad de niveles que sondeará el algoritmo Minimax de cara a calcular su jugada.


## NETLOGO FEATURES

La implementación interna de estados del tablero y alternativas viables se manejan mediante matrices en NetLogo.

Mediante el uso de la extensión de matrices de NetLogo:

    extensions [matrix]

## CREDITS AND REFERENCES (CRÉDITOS Y REFERENCIAS)

Este modelo forma parte del primer trabajo en grupo de la asignatura "Inteligencia Artificial" del tercer año de curso de 2015-2016 de Ingeniería Informática: Ingeniería de Computadores en la E.T.S.I.I. de la Universidad de Sevilla (España).

La generación de terreno irregular totalmente aleatorio es una adaptación del ejemplo citado en la sección de Moelos relacionados.


## COPYRIGHT AND LICENSE (CRÉDITOS Y LICENCIA)

Copyright 2015 by:
**Juán Carlos Manjón Velázquez**
**Rafael Torres Malpartida**
**Oscar Garrido Murias.**

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Uri Wilensky at uri@northwestern.edu.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.2.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
